/*
 * Copyright (c) 2014 Appfire Technologies, Inc.
 * All rights reserved.
 *
 * This software is licensed under the provisions of the "Standard EULA" from the
 * "Atlassian Marketplace Terms of Use" as a "Marketplace Product". Reference is:
 * http://www.atlassian.com/licensing/marketplace/termsofuse.
 * The this case, the "Publisher" is Appfire Technologies, Inc.
 * and the "Marketplace Product" is the Table Plugin for Confluence.
 *
 * See the LICENSE file for more details.
 */

@Grab(group='org.swift.tools', module='gint', version='2.6.0-SNAPSHOT')

import org.swift.tools.*

includeTool << Helper             // helper utilities
includeTool << GintForConfluence  // gint framework with extensions for Confluence macro testing

gint.initialize(this) // required

def wiki = helper.getBooleanParameterValue('wiki')
if (wiki) {
    gint.setSpaceKey(gint.getSpaceKey() + 'WIKI')  // different space for wiki test
    gint.setSpaceName(gint.getSpaceName() + '-wiki')
}

def info = gint.getServerInfoWithVerify()  // verify server is available, otherwise end test
def v58 = helper.compareSeparatedGreaterThanOrEqual(info?.version,'5.8')
def v59 = helper.compareSeparatedGreaterThanOrEqual(info?.version,'5.9')
//TBL-55: To enable horizontal scrolling for bigger tables, wrapping table in <div class="table-wrap">, with this change the space are not rendering a &nbsp; rather shows as ?| in the generated files but does accept when given in data
def spacePattern = v59 ? '':'&nbsp;'
//helper.logWithFormat("info.url",info.url)
boolean onDemand = helper.getBooleanParameterValue('onDemand', false)
if (onDemand) {
    user = helper.getParameterValue('onDemandUser')
}

// Confluence convenience definitions
def space = gint.getSpaceKey() // space key
def home = gint.getParent()

mpBase = 'https://marketplace.atlassian.com/rest/1.0/plugins'
def mp = [
    cache: "${mpBase}/org.swift.confluence.cache/downloads?startDate=2013-01-01&endDate=2013-12-31", // 2013 data only
    table: "${mpBase}/org.swift.confluence.table/downloads?startDate=2013-01-01&endDate=2013-12-31", // 2013 data only
    table2014toNow: "${mpBase}/org.swift.confluence.table/downloads?startDate=2014-01-01&numVersions=10", // 2014 on ward
]
// bobswift.atlassian.net
jiraBase = 'https://bobswift.atlassian.net/rest/api/latest'

// this API requires admin permissions which is important to test session cookies but fails if administrator login is required
// set excludeLevel=upm to exclude these tests when administrator login is set for an installation
def pluginKey = 'org.swift.confluence.table'
def localRestUrl = "rest/plugins/latest/${pluginKey}-key"


// Saved information from
saved = [:]
def jsonData = [
    // arrays
    array: '{"total":{"sum":7147,"monthlies":[ {"month":"2013-07","sum":712},{"month":"2013-08","sum":578} ],"weeklies":[ {"week":"2013-W14","sum":186},{"week":"2013-W17","sum":135} ]}}',
    arrayRoot: '[ {aaa:1,bbb:2,ccc:3},{aaa:5,bbb:6} ]',
    differentFields: '{"monthlies":[ {"month":"2013-07","sum":712},{"month":"2013-08","sum2":578}] }',
    arrayFieldPaths: '{"monthlies":[ {"v1": { "month":"2013-07","sum":712} },{"v1": {"month":"2013-08","sum":578} }] }',

    // key/value
    simple: '{"animals":{"dogs": 53, "horses": 15, "cats": 62, "gerbils": 9} }',
    fieldPaths: '{"Vet 1": {"animals":{"dogs": 53, "horses": 15, "cats": 62, "gerbils": 9, "white cats":5} }, "Vet 2": {"animals":{"dogs": 43, "horses": 25, "cats": 72, "gerbils": 19, "white cats":15} } }',
]

defaultTableIndicator = ~/<table id="TBL\d+/ // output that is a standard table with a default id
keyValueIndicator  = ~"<th [^>]*>Key\\s*</th>\\s*<th [^>]*>Value\\s*</th>" // key value header line

gint.addSetUp([
    gint.getAddSpaceTestcase(),
])

gint.addTearDown([
    gint.getRemoveSpaceTestcase(),
])

// Base array support
gint.add('array', [
    [macro: 'json-table', ext: 'Empty', description: 'Empty or null result, nothing shows',
        body: '',
        failData: ['<table'],
    ],
    [macro: 'json-table', ext: 'EmptyList', description: 'Empty or null result, nothing shows',
        body: '{"monthlies":[]}',
        parameters: [
            url: mp.table,
            paths: 'monthlies',
        ],
        expected: false,
        data: ['No results for path'],
    ],
    [macro: 'json-table', ext: 'ElementList', description: 'List of simple elements',
        body: '{"labels":["label1", "label2"]}',
        parameters: [
            paths: 'labels',
        ],
        data: [
            defaultTableIndicator,
            spacePattern,
            'label1', 'label2',
        ],
        failData: ['>&amp;nbsp;'], // should get translated by our code back to required space
    ],
    [macro: 'json-table', ext: 'ElementListWiki', description: 'List of simple elements',
        body: '["label1", "label2"]',
        parameters: [
            output: 'wiki',
        ],
        data: [
            defaultTableIndicator,
            spacePattern, // wiki rendering produces this from &nbsp which shows a blank header row
            'label1', 'label2',
        ],
    ],
    [macro: 'json-table', ext: 'ElementListWiki2', description: 'List of simple elements',
        body: '[["xxx1"], ["xxx2"]]',
        parameters: [
            output: 'wiki',
        ],
        data: [
            defaultTableIndicator,
            spacePattern, // wiki rendering produces this from &nbsp which shows a blank header row
            defaultTableIndicator, spacePattern, '>xxx1',
            defaultTableIndicator, spacePattern, '>xxx2',
        ],
    ],
    [macro: 'json-table', ext: 'BlankPathArray', description: 'We now allow blank paths to default to $',
        body: '[ {aaa:111} ]',
        parameters: [paths: ''],
        data: ['>Aaa<', '>111<'],
    ],
    [macro: 'json-table', ext: 'PathNotFound',
        body: jsonData.array,
        parameters: [paths: 'NOT_FOUND'],
        expected: false,
        data: ['No results for path'],
    ],
   /* This test-case should produce a table with sum value, but not sure why we are expecting no table should appear
    [macro: 'json-table', ext: 'PathNotArray',
        body: jsonData.array,
        parameters: [paths: 'total.sum'],
        failData: ['<table'],
    ],*/
    [macro: 'json-table', ext: 'PathNotArray',
        body: jsonData.array,
        parameters: [paths: 'total.sum'],
        data: [ordered: [
          ~/[.*total.*][.*sum.*]/,'>7147<',
        ]],
    ],
    [macro: 'json-table', ext: 'Array',
        body: jsonData.array,
        parameters: [paths: 'total.monthlies'],
        data: [ordered: [
           defaultTableIndicator, // make sure table id is auto generated
            '>Month<', '>Sum<',
            '>2013-07<', '>712<',
            '>2013-08<', '>578<',
        ]],
    ],
    [macro: 'json-table', ext: 'DifferentFields', description: 'Array elements do not have all the same fields',
        body: jsonData.differentFields,
        parameters: [paths: 'monthlies', capitalize: false],
        data: [ordered: [
            '<table',
            '>month<', '>sum<', '>sum2<',
            '>2013-07<', '>712<', spacePattern,
            '>2013-08<', spacePattern, '>578<',
        ]],
    ],
    [macro: 'json-table', ext: 'ArrayFieldPaths', description: 'Field path inside the array elements',
        body: jsonData.arrayFieldPaths,
        parameters: [
            paths: 'monthlies',
            fieldPaths: 'v1.month, v1.sum',
            stripQualifiers: true,
        ],
        data: [ordered: [
            '<table',
            '>Month<', '>Sum<',
            '>2013-07<', '>712<',
            '>2013-08<', '>578<',
        ]],
    ],
    [macro: 'json-table', ext: 'ArrayRoot',
        body: jsonData.arrayRoot,
        parameters: [
            paths: '$',
            fieldPaths: 'aaa,bbb,ccc', // otherwise you get random order
            capitalize: false,
        ],
        data: [ordered: [
            '>aaa<', '>bbb<', '>ccc<',
            '>1<', '>2<', '>3<',
            '>5<', '>6<',
        ]],
    ],
    //TBL-461 : If value is wikilink, instead of considering it as json data, render as wikilink.
    [macro: 'json-table', ext: 'Wikilinks', description: 'Render wikilink well',
        body: '{"wikilinks":"[TBL-461|https://bobswift.atlassian.net/wiki/questions/121012315]"}',
        parameters: [output: 'wiki'],
        data: ['<a href="https://bobswift.atlassian.net/wiki/questions/121012315" class="external-link" rel="nofollow">TBL-461</a>'],
    ],
])

// Session cookies and url access
// note that this cannot be inside a future macro as there is no session to use

/*  This approach will not work until Confluence rest api for doing the render is available
gint.add('cookies', [
    [name: 'sessionCookie', action: 'renderRequest', description: 'Create a session cookie as if you were logged in',
        parameters: [space: space, cookies: gint.getOutputFile('cookies')],
        cookies: gint.getOutputFile('cookies'),
        successClosure: { testcase ->
            def cookies = gint.readFile(testcase)  // read the output file
            helper.logWithFormat('cookies', cookies)
            return true
        },
        output: [
            file: gint.getOutputFile('cookies'),
            data: [
                ~/.*JSESSIONID=[A-F0-9]* /,
            ],
        ],
    ],
])
    */

// regular url, relative not starting with a slash, and relative starting with a slash
['', 'Relative', 'RelativeStartWithSlash'].each { type ->
    def url = (type == 'Relative') ? localRestUrl : (type == 'RelativeStartWithSlash') ? "/${localRestUrl}" : "${info.url}/${localRestUrl}"

    gint.add("cookies" + type, [
        // Render of macro by itself is going to show an authorization error. However, rendering the page after login should be fine.
        [macro: 'json-table', ext: 'LocalUrl' + type, description: 'Url to same Confluence - verify session being used, no password required',
            level: onDemand ? false : 'upm', // use excludeLevel=upm if admin login is required
            //depends: ['sessionCookie'],
            parameters: [url: url, id: 'cookie', capitalize: false],
            expected: false,
            data: ['401', 'User is not authorized'],
            //startClosure: { testcase ->
            //    testcase.cmd += ' -v --cookies ' + gint.getOutputFile('cookies')
            //},
            //data: [ordered: [
            //    '<table id="cookie"',
            //    '>key<', '>' + pluginKey + '<',
            //]],
        ],
        [name: 'json-tableLocalUrlVerify' + type, action: 'renderRequest', description: 'Render page to verify user not required for local url',
            level: onDemand ? false : 'upm', // use excludeLevel=upm if admin login is required
            depends: ['json-tableLocalUrl' + type],
            space: space,
            title: 'json-tableLocalUrl' + type,
            data: [ordered: [
                '<table id="cookie"',
                '>key<', '>' + pluginKey + '<',
            ]],
        ],
    ])
}

// key/value - not an array
gint.add('key', [
    [macro: 'json-table', ext: 'Simple', description: 'Just key and values for simple json object, should be sorted by key',
        body: jsonData.simple,
        parameters: [
            paths: 'animals',
            autoNumber: true,
            autoTotal: true,
            capitalize: false,
        ],
        data: [ordered: [
           ~/<table id="TBL\d+/, // make sure table id is auto generated
           keyValueIndicator,
            '>cats<',   '>62<',
            '>dogs<',   '>53<',
            '>gerbils<','>9<',
            '>horses<', '>15<',
        ]],
    ],
    [macro: 'json-table', ext: 'BlankPath', description: 'We now allow blank paths to default to $',
        body: '{aaa:111}',
        data: [keyValueIndicator, '>Aaa<', '>111<'],
    ],
    [macro: 'json-table', ext: 'FieldPaths', description: 'Key and values with default handling',
        body: jsonData.fieldPaths,
        parameters: [
            paths: '$',
            fieldPaths: 'animals.cats,animals.dogs,animals.horses,animals.gerbils',
        ],
        data: [ordered: [
           ~/<table id="TBL\d+/, // make sure table id is auto generated
            '>Key<',   '>Animals.cats<', '>Animals.dogs<', '>Animals.horses<', '>Animals.gerbils<',
            '>Vet 1<', '>62<', '>53<', '>15<', '>9<',
            '>Vet 2<', '>72<', '>43<', '>25<', '>19<',
        ]],
    ],
    //TBL-464
    [macro: 'json-table', ext: 'PathWithSpace', description: 'Key and values with better headings and sorted descending on key',
        body: jsonData.fieldPaths,
        parameters: [
            paths: '$',
            fieldPaths: 'animals.cats,animals.dogs,animals.horses,animals.gerbils,animals.[\'white cats\']',
            autoNumber: true,
            autoTotal: true,
            sortDescending: true,
            stripQualifiers: true,
        ],
        data: [ordered: [
           ~/<table id="TBL\d+/, // make sure table id is auto generated
            '>Key<',   '>Cats<', '>Dogs<', '>Horses<', '>Gerbils<', '>White Cats<',
            '>Vet 2<', '>72<', '>43<', '>25<', '>19<','>15<',
            '>Vet 1<', '>62<', '>53<', '>15<', '>9<','>5<',
        ]],
    ],
    [macro: 'json-table', ext: 'FieldPathsEnhanced', description: 'Key and values with better headings and sorted descending on key',
        body: jsonData.fieldPaths,
        parameters: [
            paths: '$',
            fieldPaths: 'animals.cats,animals.dogs,animals.horses,animals.gerbils',
            autoNumber: true,
            autoTotal: true,
            sortDescending: true,
            stripQualifiers: true,
        ],
        data: [ordered: [
           ~/<table id="TBL\d+/, // make sure table id is auto generated
            '>Key<',   '>Cats<', '>Dogs<', '>Horses<', '>Gerbils<',
            '>Vet 2<', '>72<', '>43<', '>25<', '>19<',
            '>Vet 1<', '>62<', '>53<', '>15<', '>9<',
        ]],
    ],
    [macro: 'json-table', ext: 'Automatic', description: 'Automatic expansion with wiki',
        body: jsonData.array,
        parameters: [output: 'wiki'],
        data: [ordered: [
            defaultTableIndicator, // make sure table id is auto generated
            keyValueIndicator,
            '>Total <', defaultTableIndicator,
            '>Monthlies <', defaultTableIndicator, '>Month <', '>Sum <',
            '>2013-08 <', '>578 <',
            '>Sum <', '7147 ',
            '>Weeklies <', defaultTableIndicator, '>Sum <', '>Week <',
            '>186 <', '>2013-W14 <',
        ]],
    ],
    [macro: 'json-table', ext: 'AutomaticFieldPaths', description: 'Automatic expansion with wiki',
        body: jsonData.array,
        parameters: [output: 'wiki', fieldPaths: 'monthlies', showWiki: true],
        data: [ordered: [
            defaultTableIndicator,
            '>Key', '>Monthlies',
            '>Total <', defaultTableIndicator,
            '>Month <', '>Sum <',
            '>2013-07 <', '>712 <',
            '>2013-08 <', '>578 <',
        ]],
    ],
])

gint.add('jsonp', [
    [macro: 'json-table', ext: 'JSONP', description: 'Handle JSONP encoded json',
        body: 'xxx( { "aaa": "bbb" } );',
        data: [
            '>Key', '>Value',
            '>Aaa', '>bbb',
        ],
    ],
])

gint.add('escaping', [
    [macro: 'json-table', ext: 'Slash', description: 'Slash should not be escaped',
        body: '{ "plan": [ { "link": { "rel": "http://yyyy" } } ], "plan2": [ { "link": { "rel/xxx": "http://yyyy" } } ] }',
        data: ['http://yyyy'],
    ],
    [macro: 'json-table', ext: 'SlashArray', description: 'Slash should not be escaped',
        body: '[ {"rel": "http://yyyy", "rel/xxx": "http://yyyy"} ]',
        data: ['http://yyyy', 'Rel/xxx'],
        failData: ['\\/'],
    ],
])

gint.add('errors', [
    [macro: 'json-table', ext: 'BadArrayReference', description: 'Error message should be reasonable',
        body: '{"expand": "projects","projects": [ ]}',
        parameters: [
            paths: 'projects[0]',
        ],
        expected: false,
        data: ['Index out of bounds when evaluating path'], //Changed message, because JSONPath 2.0.0, was giving slightly different message than JSONPath 0.9.1
    ],
])
Calendar c = Calendar.getInstance()
int year  =c.get(Calendar.YEAR)
int month = c.get(Calendar.MONTH)
lastMonth = month -1
yearMonth = ">"+ year+"-"+(month<=9 ? ("0"+ month):month)+"<"
yearLastMonth = ">"+year+"-"+(lastMonth<=9 ? ("0"+ lastMonth) : lastMonth)+"<"
helper.logWithFormat('today:',yearMonth )
helper.logWithFormat('lastmonth:', yearLastMonth)
gint.add('mp', [
    [macro: 'json-table', ext: 'Cache', description: 'Use marketplace URL to get cache downloads 2013 - by month',
        parameters: [
            paths: 'total.monthlies',
            url: mp.cache,
            autoNumber: true,
            autoTotal: true,
            autoSort: true,
            sortColumn: 1,
            headingAugments: 'Month, Cache'
        ],
        data: [ordered: [   // data may not be sorted,
            '>Month<', '>Cache<',
            '>2013-07<', '>714<',
            '>2013-08<', '>578<',
        ]],
    ],
    [macro: 'json-table', ext: 'Table', description: 'table downloads 2013 - by week',
        parameters: [
            paths: 'total.weeklies',
            url: mp.table,
            autoNumber: true,
            autoTotal: true,
            //autoSort: true,
            //sortColumn: 1,
            headingAugments: 'Week, Table',
            columns: '2,1',
            sortPaths: 'week',
        ],
        data: [ordered: [
            '>Week<', '>Table<',
            '>2013-W01<', '>272<', // now data changed to 272, earlier it was 327
            '>2013-W02<', '>329<',
        ]],
    ],

    [macro: 'json-table', ext: 'Multiple', description: 'Should produce 2 tables, by week and month, ordered properly',
        parameters: [
            paths: 'total.monthlies,total.weeklies',
            url: mp.table,
            autoNumber: true,
            autoTotal: true,
            sortPaths: 'month,week',
            fieldOrderRegexPatterns: 'month,week', // this ensures that month and week are before sum in the tables
            headingAugments: 'Period,Sum',
            id: 'month,week',  // set the table ids specifically
        ],
        data: [ordered: [
            '<table id="month"',
            '>Period<', '>Sum<',
            '>2013-02<', '>1551<',
            '-03','-04','-05','-06','-07','-08','-09','-10','-11','-12',

            '<table id="week"',
            '>Period<', '>Sum<',
            '>2013-W01<', '>272<',//data now changed to 272, earlier it was 327
            '>2013-W02<', '>329<',
            '-W03','-W04','-W05','-W06','-W07','-W08','-W09','-W10','-W11','-W12','-W13', // that is probably enough
        ]],
    ],
    [macro: 'json-table', ext: 'TableByVersion', description: 'table downloads 2013 - by week',
        parameters: [
            paths: 'versions',
            url: mp.table2014toNow,
            autoNumber: true,
            // autoTotal: true,
            autoSort: true,
            sortColumn: 1,
            sortDescending: true,
            columns: '2,1',
            output: 'wiki',
        ] + (v58 ? [headingAugments: 'Table, Version',
            augments: /"%[%json-table:paths=monthlies%!%id=%1%%!%sortColumn=1%!%headingAugments=Month,Total%]%%2%%[%json-table%]%",/] : [headingAugments: 'Version, Table',
            augments: /,"%[%json-table:paths=monthlies%!%id=%2%%!%sortColumn=1%!%headingAugments=Month,Total%]%%1%%[%json-table%]%"/]),
        data: [ordered: [   // data may not be sorted,
            (v58 ? ['>Table <', '>Version <'] : ['>Version <', '>Table <']) +
            yearMonth,
            yearLastMonth,
        ]],
    ],
    [macro: 'json-table', ext: 'TableByVersionWikiShorthand', description: 'Same except use wiki shorthand for augment',
        parameters: [
            paths: 'versions',
            url: mp.table2014toNow,
            autoNumber: true,
            // autoTotal: true,
            autoSort: true,
            sortColumn: 1,
            sortDescending: true,
            columns: '2,1',
            output: 'wiki',
        ] + (v58 ? [headingAugments: 'Table, Version',
            augments: /,"aaaaa:MACRO:json-table(paths=monthlies;id=%1%;sortColumn=1;headingAugments=Month,Total):BODY:%2%:END:bbbbb"/] : [headingAugments: 'Version, Table',
            augments: /,"aaaaa:MACRO:json-table(paths=monthlies;id=%2%;sortColumn=1;headingAugments=Month,Total):BODY:%1%:END:bbbbb"/]),
       data: [ordered:
            (v58 ? ['>Table <', '>Version <'] : ['>Version <', '>Table <']) +
            [
                yearMonth,
                yearLastMonth,
            ]],
    ],
])

// JIRA examples taken with data from bobswift.atlassian.net
gint.add('jira', [
    [macro: 'json-table', ext: 'Issues', description: 'get issues from jira, plus subfields and array subfields',
        parameters: [
            url: "${jiraBase}/search?jql=key%20in%20(TBL-174%2C%20TBL-300)%20order%20by%20key", // one issue has multiple releases
            paths: 'issues',
            fieldPaths: 'key,fields.summary,fields.issuetype.name,fields.issuetype.id,fields.fixVersions[*].name',
        ],
        data: [ordered: [
            '>Key<', '>Fields.summary<', '>Fields.issuetype.name', '>Fields.issuetype.id', 'Fields.fixVersions[*].name',
            '>TBL-174<', '>Bug<', '4.4.0','5.1.0',
            '>TBL-300<', '>New Feature<', '6.3.0',
        ]],
    ],
    [macro: 'json-table', ext: 'IssuesDefault', description: 'get issues from jira, plus subfields and array subfields',
        parameters: [
            output: 'wiki',
            showWiki: true,
            escape: true,
            url: "${jiraBase}/search?jql=key%20in%20(TBL-174%2C%20TBL-300)%20order%20by%20key", // one issue has multiple releases
            paths: 'issues',
            fieldPaths: 'key,fields',
        ],
        data: [ordered: [
            '>Key <', '>Fields <',
            '>TBL-174 <',
            '>Issuetype', '>Description', 'A problem which ', '>Name', '>Bug',
            '>Priority', '>Name', '>Major',
            '>Project', '>Name', '>Advanced Tables for Confluence', '>ProjectCategory', '>Description', '>Add-ons for Confluence',
            '>TBL-300 <',
            '>Project', '>Name', '>Advanced Tables for Confluence', '>ProjectCategory', '>Description', '>Add-ons for Confluence',
        ]],
        failData: ['json-table: Error'],  // indicates an error
    ],
    [macro: 'json-table', ext: 'IssuesDefault2', description: 'get issues from jira, plus subfields and array subfields',
        parameters: [
            output: 'wiki',
            showWiki: true,
            escape: true,
            url: "${jiraBase}/search?jql=key%20in%20(TBL-174%2C%20TBL-300)%20order%20by%20key", // one issue has multiple releases
            paths: 'issues',
            fieldPaths: 'fields.description',
        ],
    ],
])

// imbedded macro either via default json handling for wiki or augments
gint.add('macro', [
    [macro: 'json-table', ext: 'MacroDefault', description: 'Let macro turn all json into tables',
        parameters: [
            output: 'wiki',
            showWiki: true,
            escape: true,
        ],
        body: '{ "aaa {escapeThis}" : { "bbb {escapeThis}" :  "ccc {escapeThis}" } }',
        data: [ordered: [
            defaultTableIndicator, // make sure table id is auto generated
            keyValueIndicator,
            '>Aaa {escapeThis} <', defaultTableIndicator, keyValueIndicator,
            '>Bbb {escapeThis} <', '>ccc {escapeThis}',
        ]],
    ],
    [macro: 'json-table', ext: 'MacroDefaultArray', description: 'Let macro turn all json into tables',
        parameters: [
            output: 'wiki',
            showWiki: true,
            escape: true,
        ],
        body: '{ "aaa" : [ {"bbb":"ccc"}, {"bbb":"ddd"} ] }',
        data: [ordered: [
            defaultTableIndicator, // make sure table id is auto generated
            keyValueIndicator,
            '>Aaa <', defaultTableIndicator,
            '>Bbb <', '>ccc', '>ddd',
        ]],
    ],
    [macro: 'json-table', ext: 'MacroDefaultArrayEscape', description: 'Let macro turn all json into tables',
        parameters: [
            output: 'wiki',
            showWiki: true,
            escape: true,
        ],
        body: '{ "aaa" : [ {"bbb{}":"ccc{}"}, {"bbb{}":"ddd{}"} ] }', //jsonPath 2.0.0 do not allow space in path
        data: [ordered: [
            defaultTableIndicator, // make sure table id is auto generated
            keyValueIndicator,
            '>Aaa <', defaultTableIndicator,
            '>Bbb{} <', '>ccc{} ', '>ddd{} ',
        ]],
    ],
    [macro: 'json-table', ext: 'Macro', description: 'Use wiki shorthand for augment',
        parameters: [
            augments: /,"aaaaa:MACRO:json-table(output=wiki;capitalize=true;sortDescending=true;escape=true):BODY:%2%:END:bbbbb"/,
            output: 'wiki',
            showWiki: true,
            escape: true,
        ],
        body: '{ "aaa {escapeThis}" : { "bbb {escapeThis}" :  "ccc {escapeThis}" } }',
        data: [ordered: [
            defaultTableIndicator, // make sure table id is auto generated
            keyValueIndicator,
            '>Aaa {escapeThis} <', '>aaaaa', defaultTableIndicator, keyValueIndicator,
            '>Bbb {escapeThis} <', '>ccc {escapeThis}',
            'bbbbb',
        ]],
    ],
    [macro: 'json-table', ext: 'MacroDefaultNoEscape', description: 'Let macro turn all json into tables',
        parameters: [
            output: 'wiki',
            showWiki: true,
            //escape: true,
        ],
        body: '{ "aaa {escapeThis}" : { "bbb {escapeThis}" :  "ccc {escapeThis}" } }',
        expected: false,
        data: ['Unknown macro'],
    ],
    [macro: 'json-table', ext: 'MacroNoBody', description: 'No body in macro',
        parameters: [
            augments: /,":MACRO:attachment-table():END:",":MACRO:attachment-table(xxx):END:"/,
            output: 'wiki',
            showWiki: true,
            escape: true,
            columns: '1,2,3',
        ],
        //body: '{ "aaa" : { "bbb" :  "ccc" } }',
        body: '{ "aaa" : "bbb" }',
        data: [ordered: [
            '|Aaa |{attachment-table:} |{attachment-table:xxx} |', // from show wiki markup
        ]],
    ],
    // embedded multiples not supported, even multiple on line not working
    /*
    [macro: 'json-table', ext: 'MacroMultiple', description: 'Multiple embedded macros',
        parameters: [
            augments: /,":MACRO:json-table(output=wiki):BODY:{"xxx":"yyy"}:END:xxxxx:MACRO:attachment-table():END:"/,
            output: 'wiki',
            showWiki: true,
            escape: true,
        ],
        //body: '{ "aaa" : { "bbb" :  "ccc" } }',
        body: '{ "aaa" : "bbb" }',
        data: [ordered: [
        ]],
    ],
    */
])

gint.add('tbl-777', [
    [macro: 'json-table', ext: 'TBL-777',
        parameters: [
            paths: 'AAA',
        ],
        body: '''
            {
                "AAA": [
                    {
                        "BBB": [
                            {
                                "CCC": 12345,
                                "DDD": "abc",
                            },
                        ],
                    },
                ]
            }
        '''.stripIndent().trim(),
       // body: '{ "AAA": [ { "BBB": [ { "CCC": 12345, "DDD": "abc", }, ], }, ] }',
        data: [ordered: [
            'BBB',
            /{&quot;CCC&quot;:12345,&quot;DDD&quot;:&quot;abc&quot;}/,
        ]],
        failData: [
            /'{'/,  // single quotes were being added
            /}'/,
        ],
    ],
    [macro: 'json-table', ext: 'TBL-777Multiple',
        parameters: [
            paths: 'AAA',
            fieldsPaths: 'number,BBB',
            augments: ',%[%json-table%]%%BBB%%[%json-table%]%',
            output: 'wiki',
        ],
        body: '''
            {
                "AAA": [
                    {
                        "BBB": [
                            {
                                "CCC": 12345,
                                "DDD": "abc",
                            },
                            {
                                "CCC": 67890,
                                "DDD": "def",
                            },
                        ],
                        "number": 111,
                    },
                    {
                        "BBB": [
                            {
                                "YYY": 123456789
                            },
                        ],
                        "number": 222,
                    },
                ]
            }
        '''.stripIndent().trim(),
        data: [ordered: [
            'Number', 'BBB',
            '111', 'DDD', 'CCC',
            'abc', '12345',
            'def', '67890',
            '222', 'YYY',
            '123456789',
        ]],
    ],
])

gint.add('errors', [
    [macro: 'json-table', ext: 'ParseError',
        level: 2,
        body: '{"list":[ {"aaa"bbb"ccc>"}} ]',
        parameters: [paths: 'list'],
        expected: false,
        data: ['Error parsing JSON', 'Unexpected character'],
        failData: ['org.jayway', 'net.minidev'],
    ],
])

// Most XSS testing is covered because we use the csv under the covers
gint.add('xss', [
    [macro: 'json-table', ext: 'Xss',
        level: 99,
        body: '{"list":[ {"field":"<script>alert(\\"BAD_JSON\\");</script>"}, {"field":"XXX"} ]}',
        parameters: [
            paths: 'list',
            id: 'xxx"><script>alert("BAD_ID")</script><table id="',  // this gets through
        ],
        data: [ordered: [
            '>Field<',
            helper.htmlEncode('<script>'),
            '>XXX<',
        ]],
        failData: ['</script></td>'],
    ],
])

gint.finalizeTest() // final preparations for running tests
