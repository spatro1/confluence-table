/*
 * Copyright (c) 2005, 2013 Bob Swift Software, LLC.
 * All rights reserved.
 *
 * This software is licensed under the provisions of the "Standard EULA" from the
 * "Atlassian Marketplace Terms of Use" as a "Marketplace Product". Reference is:
 * http://www.atlassian.com/licensing/marketplace/termsofuse.
 * The this case, the "Publisher" is Bob Swift Software, LLC
 * and the "Marketplace Product" is Table Plugin for Confluence.
 *
 * See the LICENSE file for more details.
 */

/**
 * Copyright (c) 2005, 2012 Bob Swift
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *              notice, this list of conditions and the following disclaimer in the
 *            documentation and/or other materials provided with the distribution.
 *     * The names of contributors may not be used to endorse or promote products
 *           derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Created on April 8, 2006 by Bob Swift
 */

package org.swift.confluence.table;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.swift.confluence.macroutil.MacroInfo;
import org.swift.confluence.scriptutil.Constants;
import org.swift.confluence.scriptutil.LicensedScriptMacro;
import org.swift.confluence.scriptutil.ScriptUtils;
import org.swift.confluence.scriptutil.TableHelper;
import org.swift.confluence.scriptutil.TableStyleHelper;
import org.swift.confluence.tablesorter.TableSorter;
import org.swift.confluence.tablesorter.TableUtil;

import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.templates.PageTemplateManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.SubRenderer;
import com.atlassian.upm.api.license.PluginLicenseManager;
import com.atlassian.webresource.api.assembler.PageBuilderService;

/**
 * This macro provides for column sorting for a HTML table on a page including ones created by wiki notation - the sort function is the same as provided by the
 * sql and csv macros.
 */
public class TablePlusMacro extends LicensedScriptMacro {

    protected final boolean EXEMPT_DEVELOPER_LICENSE = true;
    protected final PageBuilderService pageBuilderService; // for js resource
    // protected XhtmlContent xhtmlContent;
    protected final SubRenderer subRenderer;

    protected static final String HTML_TABLE_START = "<table";
    protected static final int HTML_TABLE_START_LENGTH = HTML_TABLE_START.length();
    protected static final RenderMode RENDER_MODE = RenderMode.allow(RenderMode.F_MACROS | RenderMode.F_RESOLVE_TOKENS);

    protected static final Pattern ID_ATTRIBUTE_PATTERN = Pattern.compile("(\\Wid=['\"])([^'\"]*)(['\"])"); // \W is non word character

    private String tableId = null; // just incase user needs to control this directly instead of defaulting to parameter

    /**
     * Constructor
     *
     * @param macroAssistant
     */
    protected TablePlusMacro(final SettingsManager settingsManager, final BootstrapManager bootstrapManager, final PageTemplateManager templateManager,
            final SpaceManager spaceManager, final PageManager pageManager, final PermissionManager permissionManager,
            final AttachmentManager attachmentManager, final LocaleManager localeManager, final I18NBeanFactory i18NBeanFactory,
            final PluginLicenseManager licenseManager, final PageBuilderService pageBuilderService, final XhtmlContent xhtmlContent,
            final SubRenderer subRenderer) {
        super(settingsManager, bootstrapManager, templateManager, spaceManager, pageManager, permissionManager, attachmentManager, localeManager,
                i18NBeanFactory, licenseManager, xhtmlContent);
        this.pageBuilderService = pageBuilderService;
        // this.xhtmlContent = xhtmlContent;
        this.subRenderer = subRenderer;
    }

    /**
     * Should we allow use for Confluence developer license even if no valid plugin license?
     *
     * @return true to allow use or false to report normal errors for invalid licenses
     */
    @Override
    protected boolean exemptDeveloperLicense() {
        return EXEMPT_DEVELOPER_LICENSE;
    }

    @Override
    protected String getExtension() {
        return "";
    }

    @Override
    protected String getMacroName() {
        return "table-plus";
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.RICH_TEXT;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.INLINE;
    }

    // @Override
    // public ParameterAssistant getParameterAssistant() {
    // log.debug("get parameter assistant");
    // return macroAssistant.getParameterAssistant();
    // }

    /**
     * Primary processing
     */
    @Override
    protected String execute(final MacroInfo info) throws MacroExecutionException {
        validateLicense(licenseManager);
        // TBL-7
        tableId = null; // in case multiple table-plus macros on page, this should be nullified otherwise all would share same id
        try {
            TableSorter tableSorter = TableUtil.setupTableSorter(new TableHelper().getTableProperties(info), pageBuilderService);
            tableSorter = TableUtil.updateSortTip(tableSorter, getStringOrTranlation(tableSorter.getSortTip(), "@org.swift.confluence.table.sortTip"));

            // log.debug("macro body: " + info.getMacroBody());
            // log.debug("macro body2: " + info.getMacroAssistant().getXhtmlContent().);
            // log.debug("rendered: " + rendered);

            // Body is already rendered on Confluence 4.x
            String rendered = info.getMacroBody();
            // log.debug("rendered: {}", rendered);

            boolean multiple = ScriptUtils.getBoolean("multiple", true, info); // process each table found, otherwise only first
            String[] idList = info.getMacroParams().getString("id", "").split(","); // default to automatically generate id

            StringBuilder builder = new StringBuilder(rendered.length());

            int startTableIndex = ScriptUtils.findTableIndex(rendered, 0);
            int endTableIndex = ScriptUtils.findMatchedEndTableIndex(rendered, startTableIndex);

            log.debug("first start table: {}, end table: {}", startTableIndex, endTableIndex);
            // if (endTableIndex > 20) {
            // log.debug("endTableIndex partial: " + rendered.substring(endTableIndex - 20, endTableIndex));
            // }

            int tableIdIndex = 0;  // loop through table ids, one for each table to be processed

            builder.append(rendered.substring(0, startTableIndex));
            StringBuilder columnStyleHtml = new StringBuilder();
            while (startTableIndex < rendered.length()) {
                if ((tableIdIndex < idList.length) && (idList[tableIdIndex].trim().length() > 0)) {
                    tableId = GeneralUtil.htmlEncode(idList[tableIdIndex].trim());
                }
                tableIdIndex++;  // increment to next table id index

                // process this table
                builder.append(processTable(info, tableSorter, rendered.substring(startTableIndex, endTableIndex)));
                log.debug("tableId:" + tableId);
                // next table if processing all tables, or set to the end
                startTableIndex = multiple ? ScriptUtils.findTableIndex(rendered, endTableIndex) : rendered.length();
                // TBL-7
                String dataBetweenTable = rendered.substring(endTableIndex, startTableIndex);
                columnStyleHtml.append(
                        TableUtil.columnStyleHtml(new TableStyleHelper().constructColumnStyleHtml(ScriptUtils.getStyles(info, "columnStyles")), tableId));
                builder.append(dataBetweenTable); // this covers the inbetween table data and the end of table data
                if (startTableIndex >= rendered.length()) {
                    break;
                }
                endTableIndex = ScriptUtils.findMatchedEndTableIndex(rendered, startTableIndex + Constants.END_TABLE.length());
                if (endTableIndex > rendered.length()) {
                    endTableIndex = rendered.length();  // shouldn't be necessary unless table is messed up
                }
                log.debug("start table: {}, end table: {}", startTableIndex, endTableIndex);
                tableId = null; // if more than 1 table in a macro, both should not share same tableid
            }
            builder.append(tableSorter.getJavaScriptForDateFunctions());  // append D type script include if necessary

            // This is moved to js code.
            // TBL-735: if export enabled then only add icons.
            // if (ScriptUtils.getBoolean("allowExport", false, info)) {
            // // TBL-7
            // builder = TableUtil.addExportDiv(builder.toString(), ScriptUtils.getBoolean("allowExport", false, info), info.getContent());
            // }
            // log.debug("table html:{}", builder.toString());
            return builder.toString();
        } catch (Exception exception) {
            log.error(info.getMacroBody(), exception); // log unexpected exception for problem determination
            throw new MacroExecutionException("Unexpected program error: " + exception.toString());
        }
    }

    /**
     * Update the table markup with everything necessary like id and styles
     *
     * @param info
     * @param tableSorter
     * @param segment - assumed to be: <table ... > ...
     *            </table>
     * @param tableId
     * @return
     * @throws MacroExecutionException
     */
    protected String processTable(final MacroInfo info, final TableSorter tableSorter, final String segment) throws MacroExecutionException {
        // FIND and update the table html element
        int endMark = segment.indexOf('>'); // end mark of <table ...>
        String originalTableId = "";
        if (endMark > 0) {
            StringBuilder builder = new StringBuilder();
            String otherTableAttributes = segment.substring(HTML_TABLE_START_LENGTH, endMark);
            // log.debug("current table id: {}, other table attributes: {}", tableId, otherTableAttributes);

            final Matcher matcher = ID_ATTRIBUTE_PATTERN.matcher(otherTableAttributes);
            if (matcher.find()) { // found an existing id attribute
                if (StringUtils.isBlank(tableId)) {
                    // case where we just leave the existing id alone by leaving tableId blank
                } else { // user requested a new id, so replace existing id with this one
                    originalTableId = tableId; // TBL-487: restore the tableId, so that it can be used in javascript
                    otherTableAttributes = matcher.replaceFirst("$1" + tableId + "$3");
                    tableId = ""; // so later processing doesn't add it to the table tag
                }
            } else if (StringUtils.isBlank(tableId)) { // blank and we didn't find an existing id set
                tableId = "TBL" + Long.toString(ScriptUtils.getUniqueId()); // need to have an id for the table
            }
            // log.debug("updated table id: {}, other table attributes: {}", tableId, otherTableAttributes);

            boolean applyParameters = true;
            builder.append(ScriptUtils.getStandardTableHtml(info, tableId, getOtherParameters(info) + otherTableAttributes, applyParameters));

            TableStyleHelper tableStyleHelper = new TableStyleHelper();
            builder.append(tableStyleHelper.constructColumnStyleHtml(ScriptUtils.getStyles(info, "columnStyles")));

            // TBL-55: Apply columnstyles later once sticky headers loaded
            // builder.append(Utils.columnGroupToBeAddedLater() ? "" : tableStyleHelper.constructColumnStyleHtml(ScriptUtils.getStyles(info, "columnStyles")));
            // log.debug("html:{}", segment.substring(endMark + 1));
            builder.append(tableStyleHelper.modifyTableRowStyles(segment.substring(endMark + 1), ScriptUtils.getStyles(info, "rowStyles"),
                    tableSorter.getHeading(), false));
            builder.append(tableSorter.getJavaScriptHtml(tableId != null && tableId.isEmpty() ? originalTableId : tableId)); // TBL-487: if tableId calculated
            // log.debug("processed table: {}", builder); // as
            return builder.toString();
        }
        // this shouldn't happen unless the html is messed up
        log.debug("Couldn't find the end of table tag: >");
        return segment;  // return unprocessed data
    }

    /**
     * get other parameters to include in the html. ALL parameters defined for this macro MUST be listed here so they are not included in HTML by default
     */
    protected String getOtherParameters(final MacroInfo info) {

        Map<String, String> parameters = info.getMacroParamsMap();
        StringBuilder output = new StringBuilder();

        for (String key : parameters.keySet()) {
            // log.debug("key: {}", key);
            if (key != null) { // not one of the macro parameters
                key = key.trim();
                if ((key.length() != 0)  //
                        && !key.equalsIgnoreCase("multiple") //
                        && !key.equalsIgnoreCase("heading") //
                        && !key.equalsIgnoreCase("footing") //
                        && !key.equalsIgnoreCase("columnTypes") //
                        && !key.equalsIgnoreCase("highlightColor") //
                        && !key.equalsIgnoreCase("highlightRow") // depreciated parameter
                        && !key.equalsIgnoreCase("enableHighlighting") //
                        && !key.equalsIgnoreCase("enableSorting") //
                        && !key.equalsIgnoreCase("sortTip") //
                        && !key.equalsIgnoreCase("sortIcon") //
                        && !key.equalsIgnoreCase("sortColumn") //
                        && !key.equalsIgnoreCase("sortDescending") //
                        && !key.equalsIgnoreCase("autoTotal") //
                        && !key.equalsIgnoreCase("autoNumber") //
                        && !key.equalsIgnoreCase("autoNumberSort") //
                        && !key.equalsIgnoreCase("allowExport") //
                        && !key.equalsIgnoreCase("columnAttributes") //
                        && !key.equalsIgnoreCase("enableHeadingAttributes") //
                        && !key.equalsIgnoreCase("totalRow") //
                        && !key.equalsIgnoreCase("class") //
                        && !key.equalsIgnoreCase("style") //
                        && !key.equalsIgnoreCase("rowStyles") //
                        && !key.equalsIgnoreCase("columnStyles") //
                        && !key.equalsIgnoreCase("columnCalculations") //
                        && !key.equalsIgnoreCase("retainRowStyleOrder") //
                        && !key.equalsIgnoreCase("width") //
                        && !key.equalsIgnoreCase("border") //
                        && !key.equalsIgnoreCase("id") //
                        && !key.equalsIgnoreCase("0") // default parameter
                        && !key.startsWith(":") // not the RAW parameter
                        && !key.startsWith("atlassian-") // somehow atlassian-macro-output-type is getting inserted with processing causing TBL-224
                        && !key.equals("@body") // not the body parameter
                ) {
                    // quote the value if it doesn't look quoted
                    output.append(" ").append(GeneralUtil.htmlEncode(key)).append("=").append("\"").append(GeneralUtil.htmlEncode(parameters.get(key)))
                            .append("\"");
                }
            }
        }
        return output.toString();
    }
}