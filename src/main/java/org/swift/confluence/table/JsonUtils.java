/*
 * Copyright (c) 2014 Appfire Technologies, Inc.
 * All rights reserved.
 *
 * This software is licensed under the provisions of the "Standard EULA" from the
 * "Atlassian Marketplace Terms of Use" as a "Marketplace Product". Reference is:
 * http://www.atlassian.com/licensing/marketplace/termsofuse.
 * The this case, the "Publisher" is Appfire Technologies, Inc
 * and the "Marketplace Product" is Table Plugin for Confluence.
 *
 * See the LICENSE file for more details.
 */

package org.swift.confluence.table;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.swift.confluence.scriptutil.Constants;
import org.swift.confluence.scriptutil.ScriptUtils;

import com.atlassian.confluence.macro.MacroExecutionException;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

public class JsonUtils {

    protected static final Logger log = LoggerFactory.getLogger("org.swift.confluence.table.JsonUtils");

    protected static final JsonPath defaultPath = JsonPath.compile("$"); // compile this once

    /**
     * Convert JSON array found within a JSON string using a JSON path. Sorting is provided as JSON data is not guarenteed to be in order. The sorting is done
     * on the host so that the results can be provided to other macros like the chart macro in a reasonable order. The sorting is type specific sorting without
     * all the bells and whistles of the display level sorting.
     *
     * @param data - raw JSON data
     * @param jsonPath - json path to an array element in the JSON data
     * @param keyList - list of keys to represent columns of data that will be produced by accessing the key in each row of the array. If no keys are provided,
     *            the default is to use all keys.
     * @param sortPathList - a relative path within the row to compare, multiple paths could be used if the first compare is equal
     * @param sortDescending - reverse direction of sorting
     * @return csv formated string suitable for displaying as csv data
     * @throws MacroExecutionException
     */
    static public String convertJsonToCsv(final String data, final JsonPath jsonPath, final List<JsonPath> fieldPathList, final List<JsonPath> sortPathList,
            final List<Pattern> fieldOrderPatternList, boolean sortDescending, boolean capitalize, boolean stripQualifiers, boolean isWiki, boolean escapeWiki)
            throws MacroExecutionException {
        String result = "";
        JSONArray rawJsonArray = getJsonArray(data, jsonPath);

        // log.debug("json path: {}, rawJsonArray: {}", jsonPath.getPath(), rawJsonArray);
        // log.debug("data: {}", data);

        // Array reference
        if (rawJsonArray != null) {
            JSONArray jsonArray = getSortedJsonArray(rawJsonArray, sortPathList, sortDescending);
            // log.debug("Sorted json array found: {}", jsonArray);
            result = toCsv(jsonArray, fieldPathList, fieldOrderPatternList, capitalize, stripQualifiers, isWiki, escapeWiki);

            // Non-array reference
        } else {
            JSONObject rawJsonObject = getJson(data, jsonPath);
            if (rawJsonObject != null) {
                result = toCsv(rawJsonObject, fieldPathList, fieldOrderPatternList, capitalize, stripQualifiers, sortDescending, isWiki, escapeWiki);
            }
        }
        // log.debug("result: {}", result);
        return result;
    }

    /**
     * Produce a comma delimited text from a JSONArray of JSONObjects. The keyList will be used to select column data. If not available, the first row will be a
     * list of keys obtained by inspecting the first JSONObject. JSONArrays found within the selected data will be represented as JSON strings.
     *
     * @param jsonArray JSONArray - expect not null
     * @return Comma delimited text or null
     * @throws MacroExecutionException
     */
    static public String toCsv(final JSONArray jsonArray, List<JsonPath> fieldPathList, List<Pattern> fieldOrderPatternList, boolean capitalize,
            boolean stripQualifiers, boolean isWiki, boolean escapeWiki) throws MacroExecutionException {

        if (jsonArray.size() > 0) { // must find a non-empty array before we create a table. Not an explicit error however, just blank.

            StringBuilder builder = new StringBuilder();

            // if a field path list is NOT provided, construct a default made up of the union of all key sets for each element
            if (fieldPathList == null) {
                Set<String> fieldSet = new LinkedHashSet<String>(); // ordered set

                for (Object element : jsonArray) {  // each row
                    JSONObject json = getJsonObject(element, null);
                    if (json != null) {
                        fieldSet.addAll(json.keySet());
                        // log.debug("json key set: {}", json.keySet().toString());
                    }
                    log.debug("field set: {}", fieldSet.toString());
                }
                fieldPathList = JsonUtils.getPathList(ScriptUtils.toSeparatedString(fieldSet, ScriptUtils.COMMA));
                sortJsonPathList(fieldPathList, fieldOrderPatternList);

                if (fieldPathList == null) {  // no key, just a simple element list: ["label1", "label2"]
                    builder.append("&nbsp;"); // Blank header
                    for (Object object : jsonArray.toArray()) {
                        builder.append('\n'); // end of row
                        appendAsString(builder, object, isWiki, escapeWiki);
                    }
                }
            }
            if ((fieldPathList != null) && (fieldPathList.size() > 0)) { // we are really going to find some data
                appendRow(builder, fieldPathList, capitalize, stripQualifiers, isWiki, escapeWiki);  // header row

                for (Object element : jsonArray) {  // each element represents a row
                    JSONObject json = getJsonObject(element, null); // json representation of the row
                    // log.debug("element: {}, json: {}", element, json);

                    if ((json != null)) {
                        boolean firstColumn = true;
                        for (JsonPath path : fieldPathList) {

                            if (!firstColumn) {
                                builder.append(',');
                            } else {
                                firstColumn = false;
                            }
                            Object value = readJsonPath(path, json);
                            // log.debug(" value: {}", value);
                            appendAsString(builder, getValueAsString(value), isWiki, escapeWiki); // handles null values
                        }
                        builder.append('\n'); // end of row
                    }
                }
            }
            // String result = builder.toString();
            // log.debug("result: {}", builder.toString());
            // return result;
            return builder.toString();
        } else {
            log.debug("JSON array is empty");
        }
        return null;
    }

    public static Object readJsonPath(JsonPath path, String json) {
        Object value;
        try {
            value = path.read(json);
        } catch (Exception e) {
            log.error(e.getMessage());
            // e.printStackTrace();
            return null;
        }
        return value;
    }

    public static Object readJsonPath(JsonPath path, JSONObject json) {
        Object value;
        try {
            value = path.read(json);
        } catch (Exception e) {
            log.error(e.getMessage());
            // e.printStackTrace();
            return null;
        }
        return value;
    }

    /**
     * Produce a comma delimited text from a JSONObject (NOT an array).
     *
     * @param jsonObject - expect not null
     * @return Comma delimited text
     * @throws MacroExecutionException
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    static public String toCsv(final JSONObject jsonObject, List<JsonPath> fieldPathList, List<Pattern> fieldOrderPatternList, boolean capitalize,
            boolean stripQualifiers, boolean sortDescending, boolean isWiki, boolean escapeWiki) {

        StringBuilder builder = new StringBuilder();
        boolean valueOnly = (fieldPathList == null) || fieldPathList.isEmpty(); // user did not request specific fields

        // header
        builder.append("\"Key\"");
        if (valueOnly) {
            builder.append(",\"Value\"");
        } else {
            for (JsonPath path : fieldPathList) {
                builder.append(",\"").append(getString(path, capitalize, stripQualifiers)).append('"');
            }
        }
        builder.append('\n');

        // body
        List<String> keyList = new ArrayList<String>(jsonObject.keySet());
        if (sortDescending) {  // sort alphabetical to have a predictable order
            Collections.sort(keyList, Collections.reverseOrder());
        } else {
            Collections.sort(keyList); // sort alphabetical to have a predictable order
        }

        for (String key : keyList) { // get a row for each key from the json object, columns based on field path selection
            // TBL-357 to capitalize the column heading
            appendAsString(builder, ScriptUtils.quoteString(capitalize ? WordUtils.capitalize(key) : key, ScriptUtils.DOUBLE_QUOTE), isWiki, escapeWiki);

            builder.append(",");
            Object value = jsonObject.get(key);
            if (value instanceof Map) {
                value = new JSONObject((Map) value);
            }
            log.debug("key:{}, value {}", key, value);
            if (value != null) {
                // log.debug("value: {}, class: {}", value.toString(), value.getClass().getName());
                if (valueOnly) {
                    appendAsString(builder, value.toString(), isWiki, escapeWiki);
                } else {
                    boolean first = true;
                    for (JsonPath path : fieldPathList) {  // columns ordered by order specified by custom in fieldPathList
                        if (!first) {
                            builder.append(',');
                        } else {
                            first = false;
                        }
                        if (value instanceof JSONObject) { // Must be a json object, otherwise blank for column
                            appendAsString(builder, path.read(value), isWiki, escapeWiki);
                        }
                    }
                }
            }
            builder.append('\n');
        }
        // log.debug("result: {}", builder.toString());
        // return result;
        return builder.toString();
    }

    /**
     * Append a row based the the strings in the list.
     *
     * @param builder
     * @param list of strings representing a row of data
     */
    static protected void appendRow(final StringBuilder builder, final List<JsonPath> pathList, boolean capitalize, boolean stripQualifiers, boolean isWiki,
            boolean escapeWiki) {
        boolean firstColumn = true;
        for (JsonPath path : pathList) {
            if (!firstColumn) {
                builder.append(',');
            } else {
                firstColumn = false;
            }
            appendAsString(builder, getString(path, capitalize, stripQualifiers), isWiki, escapeWiki);
        }
        builder.append('\n');
    }

    /**
     * Append a string representation of the object suitable for a csv column
     *
     * @param builder
     * @param object
     */
    static public void appendAsString(final StringBuilder builder, Object object, boolean isWiki, boolean escapeWiki) {
        String string = (String) ((object instanceof String) ? object : (object == null ? "" : object.toString()));
        if (string.length() > 0) {
            boolean isJson = false; // assume not json
            if (escapeWiki) {
                isJson = isJson(string.trim());
                if (!isJson) {
                    string = ScriptUtils.ensureWikiValue(string);
                }
            }
            if (isWiki) {
                isJson = isJson || (!escapeWiki && isJson(string.trim())); // calculate json if not already done
                if (!isJson) {
                    string = string.replaceAll("(?:\n|(?:\r\n)){2}", escapeWiki ? " \n" : "\\ "); // wiki notation that isn't going to fouled up a table
                }
            }
            // if not already double quoted, then double quote
            if ((string.charAt(0) != ScriptUtils.DOUBLE_QUOTE) || (string.charAt(string.length() - 1) != ScriptUtils.DOUBLE_QUOTE)) {
                string = ScriptUtils.quoteString(string, ScriptUtils.DOUBLE_QUOTE);
            }
            string = unescape(string); // unescape json slash
            builder.append(string);
        }
        // log.debug("builder as string: {}", builder.toString());
    }

    /**
     * Sort a JSONArray using provided sortPathList. Usually, only the first path is used. But if that compares equal, subsequent paths can be used to compare.
     *
     * @param jsonArray
     * @param sortPathList
     * @param sortDescending - to reverse order of result
     * @return JSONArray sorted
     */
    static public JSONArray getSortedJsonArray(final JSONArray jsonArray, final List<JsonPath> sortPathList, final boolean sortDescending) {
        if ((jsonArray != null) && (sortPathList != null)) {
            Collections.sort(jsonArray, new Comparator<Object>() {
                @SuppressWarnings({"rawtypes", "unchecked"})
                @Override
                public int compare(final Object j1, final Object j2) {
                    int comparison = 0;
                    JSONObject json1 = getJsonObject(sortDescending ? j2 : j1, null);  // switch order for descending
                    JSONObject json2 = getJsonObject(sortDescending ? j1 : j2, null);  // switch order for descending
                    if (json1 != null) {
                        if (json2 != null) {
                            for (JsonPath path : sortPathList) {
                                Object object1 = readJsonPath(path, json1);
                                Object object2 = readJsonPath(path, json2);
                                if ((object1 instanceof Comparable) && (object2 instanceof Comparable)) {
                                    comparison = ((Comparable) object1).compareTo(object2);
                                }
                                if (comparison != 0) {
                                    break;  // once non equal for a specific key, we are done
                                }
                            }
                        } else {
                            comparison = -1; // JSONObjects are < non objects
                        }
                    } else {
                        comparison = (j2 instanceof JSONObject) ? 1 : 0;
                    }
                    return comparison;
                }
            });
        }
        // log.debug("sorted by: {}, array: {}", sortPathList, jsonArray);
        return jsonArray;
    }

    /**
     * Sort a path list based on the text representation of the path and a list of patterns. Use the pattern order to match the path text.
     *
     * @param pathList - paths to order
     * @param patternList - patterns to *find* path text for comparison
     * @return the input pathList is re-ordered by this
     */
    static public void sortJsonPathList(List<JsonPath> pathList, final List<Pattern> patternList) {
        if ((pathList != null) && (patternList != null)) {
            Collections.sort(pathList, new Comparator<JsonPath>() {
                @Override
                public int compare(final JsonPath path1, final JsonPath path2) {
                    int comparison = 0;
                    for (Pattern pattern : patternList) {
                        Matcher matcher1 = pattern.matcher(path1.getPath());
                        Matcher matcher2 = pattern.matcher(path2.getPath());
                        boolean find1 = matcher1.find();
                        boolean find2 = matcher2.find();
                        if (find1 && !find2) {
                            comparison = -1;  // find 1 is less than find 2
                        } else if (find2 && !find1) {
                            comparison = 1;
                        }
                        if (comparison != 0) {
                            break;
                        }
                    }
                    return comparison;
                }
            });
        }
    }

    /**
     * Compile a string into a JSON path
     *
     * @param path as a string
     * @return json path that can be used for accessing data
     * @throws MacroExecutionException
     */
    static public JsonPath getJsonPath(final String path) throws MacroExecutionException {
        try {
            return JsonPath.compile(path);
        } catch (Exception exception) {
            throw new MacroExecutionException("Invalid path specified: " + path + ". " + getErrorMessage(exception));
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    static public JSONObject getJsonObject(final Object object, JsonPath jsonPath) {
        try {
            JSONObject json = null;
            if (object instanceof Map) {
                json = new JSONObject(((Map) object));
            } else {
                Map<String, Object> tempMap = new HashMap<String, Object>();
                if (jsonPath != null) {
                    tempMap.put(jsonPath.getPath(), object);
                } else {
                    tempMap.put("", object);
                }
                json = new JSONObject(tempMap);
            }
            return json;
        } catch (Exception e) {
            // e.printStackTrace();
            log.error(e.getMessage());
            return null;
        }
    }

    /**
     * Get a JSON element from a JSON string and path
     *
     * @param data - raw JSON string
     * @param jsonPath - path to search
     * @return json array or null of not found or not an array
     * @throws MacroExecutionException
     */
    static public JSONObject getJson(final String data, final JsonPath jsonPath) throws MacroExecutionException {
        if (!StringUtils.isBlank(data)) {
            try {
                Object jsonData = jsonPath.read(data);
                return getJsonObject(jsonData, jsonPath);  // may be null
            } catch (NullPointerException exception) {
                // ignore an NPE that indicates a json path problem parsing the json for rare json data situations. This way user gets the rest of the data. It
                // is unlikely the user can do anything about it.
                log.debug("Ignore NPE parsing data, likely data is not a json string. Data: " + data, exception);
            } catch (PathNotFoundException exception) {
                // exception.printStackTrace();
                log.error(exception.getMessage());
                throw new MacroExecutionException(getErrorMessage(exception));
            } catch (Exception exception) {
                // exception.printStackTrace();
                log.error(exception.getMessage());
                throw new MacroExecutionException(getErrorMessage(exception));
            }
        }
        return null;
    }

    static public JSONArray getJsonArray(final Object object) {
        return object instanceof JSONArray ? (JSONArray) object : null;
    }

    /**
     * Get a JSON array element from a JSON string and path
     *
     * @param data - raw JSON string
     * @param jsonPath - path to search
     * @return json array or null of not found or not an array
     * @throws MacroExecutionException
     */
    static public JSONArray getJsonArray(final String data, final JsonPath jsonPath) throws MacroExecutionException {
        if (!StringUtils.isBlank(data)) {
            try {
                Object jsonData = jsonPath.read(data);
                return getJsonArray(jsonData);  // may be null
            } catch (NullPointerException exception) {
                // ignore an NPE that indicates a json path problem parsing the json for rare json data situations. This way user gets the rest of the data. It
                // is unlikely the user can do anything about it.
                log.debug("Ignore NPE parsing data, likely data is not a json array. Data: " + data, exception);
            } catch (PathNotFoundException exception) {
                // exception.printStackTrace();
                log.error(exception.getMessage());
                throw new MacroExecutionException(getErrorMessage(exception));
            } catch (Exception exception) {
                // exception.printStackTrace();
                log.error(exception.getMessage());
                throw new MacroExecutionException(getErrorMessage(exception));
            }
        }
        return null;
    }

    /**
     * Get a compiled path list from a comma separated list of path strings
     *
     * @param paths
     * @return list of compiled paths or null
     */
    static public List<JsonPath> getPathList(final String paths) throws MacroExecutionException {
        List<JsonPath> pathList = null;
        if (!StringUtils.isBlank(paths)) {
            List<String> pathStringList = ScriptUtils.csvDataAsList(paths, ScriptUtils.COMMA, ScriptUtils.DOUBLE_QUOTE, Constants.LF);
            pathList = new ArrayList<JsonPath>();
            for (String string : pathStringList) {
                pathList.add(JsonUtils.getJsonPath(string));
            }
        }
        return pathList;
    }

    /**
     * Get a compiled patterns
     *
     * @param paths
     * @return list ofcompiled paths
     */
    static public List<Pattern> getPatternList(final String regexPatterns) throws MacroExecutionException {
        List<Pattern> patternList = null;
        if (!StringUtils.isBlank(regexPatterns)) {
            List<String> regexStringList = ScriptUtils.csvDataAsList(regexPatterns, ScriptUtils.COMMA, ScriptUtils.DOUBLE_QUOTE, Constants.LF);
            patternList = new ArrayList<Pattern>();
            for (String string : regexStringList) {
                try {
                    patternList.add(Pattern.compile(string));
                } catch (PatternSyntaxException exception) {
                    throw new MacroExecutionException("Invalid regex specified: " + string + ". " + exception.getMessage());
                }
            }
        }
        return patternList;
    }

    /**
     * Get a cleaned up exception message
     *
     * @param exception
     * @return
     */
    static public String getErrorMessage(final Exception exception) {
        String message = exception.getMessage();
        if (message == null) {
            message = exception.toString().contains("NullPointer")
                    ? "Unknown error processing construct. No other information is available. Repair and try again." : exception.toString();
            if (log.isDebugEnabled()) {
                log.debug("Error from json processing. Likely invalid construct that was mishandled by json processing. " + message, exception);
            }
        }
        return message.replace("org.jayway.jsonpath.", "").replace("net.minidev.json.parser.", "").replaceFirst(" \\S+java.lang.String\\S*", "");
    }

    /**
     * Strip off the standard head of a path so that simple paths have a usable name. This should ALWAYS be done and is not affected by stripQualifiers
     *
     * @param json path
     * @return usable string representation of a path
     */
    static protected String getString(final JsonPath path) {
        String pathString = StringUtils.replaceOnce(StringUtils.replaceOnce(path.getPath(), "$['", ""), "@.", "");
        // jsonpath 2.0.0 returns paths as $['total']['weeklies'], hence the below 2 lines added to remove [' and ]'
        pathString = StringUtils.replace(pathString, "['", ".");
        pathString = StringUtils.replace(pathString, "']", "");
        return pathString; // no regex
    }

    /**
     * Get the string value with modifications dictated by capitalize and stripAllQualifiers.
     *
     * @param path
     * @param capitalize
     * @param stripAllQualifiers
     * @return modified string value of the paht
     */
    static protected String getString(final JsonPath path, boolean capitalize, boolean stripQualifiers) {
        // always remove at least the leading qualifiers
        String pathString = stripQualifiers ? StringUtils.substring(path.getPath(), path.getPath().lastIndexOf("[") + 1, path.getPath().lastIndexOf("]"))
                : getString(path);
        pathString = pathString.replace("'", "");
        return capitalize ? WordUtils.capitalize(pathString) : pathString;
    }

    /**
     * Get value as a string even if it is a collection. Collection convert to single quoted comma separated lists.
     *
     * @param value
     * @return string
     */
    @SuppressWarnings("unchecked")
    static protected String getValueAsString(final Object value) {
        String result = "";
        if (value != null) {
            if (value instanceof JSONObject) {
                result = ((JSONObject) value).toString();
            } else if (value instanceof JSONArray) {
                result = ((JSONArray) value).toString();
            } else if (value instanceof Collection) { // not sure this is even needed anymore, leave anyway just in case
                result = toSeparatedStringSpecial((Collection<Object>) value, ScriptUtils.COMMA, ScriptUtils.SINGLE_QUOTE);
            } else if (value instanceof Map) {
                return JSONObject.toJSONString((Map<String, ?>) value);
            } else {
                result = value.toString();
            }
        }
        return result;
    }

    /**
     * Convert a list of objects to a separated string with special handling of values using getValueAsString
     *
     * @param list
     * @param separator - separator between strings
     * @return separated string
     */
    public static String toSeparatedStringSpecial(final Collection<Object> list, final String separator, final char quote) {
        boolean first = true;
        StringBuilder result = new StringBuilder();
        if (list != null) {
            for (Object object : list) {
                if (!first) {
                    result.append(separator);
                } else {
                    first = false;
                }
                String string = getValueAsString(object);
                result.append(string.contains(separator) && (list.size() > 1) ? ScriptUtils.quoteString(string, quote) : string);
            }
        }
        return result.toString();
    }

    /**
     * Test to see if a string value looks like a json string
     *
     * @param value - should be a non-null, trimmmed value
     * @return true if the value is a json object
     */
    public static boolean isJson(final String value) {
        boolean isJson = (value.startsWith("{") && value.endsWith("}")) || (value.startsWith("[") && value.endsWith("]"));
        if (isJson) {
            try {
                isJson = (null != getJson(value, defaultPath));
            } catch (Exception exception) {
                // exception.printStackTrace();
                // log.debug("expected exception: {}", exception.toString());
                isJson = false;
            }
            if (!isJson) {
                try {
                    isJson = (null != getJsonArray(value, defaultPath));
                } catch (Exception exception) {
                    // ignore, isJson already false
                    // log.debug("expected exception: {}, value: {}", exception.toString(), value);
                }
            }
        }
        // log.debug("isJson: {}, for value: {}", isJson, value);
        return isJson;
    }

    /**
     * JSONP looks like: xxx(data); . So, just strip off the function notation and see if it is json. If so, use that instead.
     *
     * @param data
     * @return data or json subset data
     */
    public static String handleJsonP(final String data) {
        if (!isJson(data)) {
            int startIndex = data.indexOf('(');
            int endIndex = data.lastIndexOf(')');
            if ((startIndex > 0) && (endIndex > startIndex)) {
                String jsonData = data.substring(startIndex + 1, endIndex).trim();
                if (isJson(jsonData)) {
                    return jsonData; // so, it looks like we found some meaningful data
                }
            }
        }
        return data;  // default is just the original data
    }

    public static String unescape(final String value) {
        return value.replaceAll("\\\\/", "/"); // unescape slash escaping
    }
}
