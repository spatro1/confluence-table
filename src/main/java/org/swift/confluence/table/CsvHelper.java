/*
 * Copyright (c) 2005, 2013 Bob Swift Software, LLC.
 * All rights reserved.
 *
 * This software is licensed under the provisions of the "Standard EULA" from the
 * "Atlassian Marketplace Terms of Use" as a "Marketplace Product". Reference is:
 * http://www.atlassian.com/licensing/marketplace/termsofuse.
 * The this case, the "Publisher" is Bob Swift Software, LLC
 * and the "Marketplace Product" is Table Plugin for Confluence.
 *
 * See the LICENSE file for more details.
 */

package org.swift.confluence.table;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.swift.confluence.macroutil.MacroInfo;
import org.swift.confluence.scriptutil.ScriptUtils;
import org.swift.confluence.scriptutil.TableStyleHelper;
import org.swift.confluence.tablesorter.TableSorter;
import org.swift.confluence.tablesorter.TableUtil;

import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.SubRenderer;

/*
 * <pre>
 * ----- Simple ----
 * csvSimple testcase
 *                         setRowAndColumnCountsFromData
 * Row data                rowColumnCountList  rowStartIndexList  valuesLength firstRowNumber
 * 0   <blank row>              1                  0
 * 1   h1, h2, h3               3                  1                                 1
 * 2   row11,                   2                  4
 * 3   row21, row22             2                  6
 * 4   row31, row32, row33      3                  8                   11       if ignoring trailing blank rows
 * 5   <blank>                  1                 11                            only if not ignoring trailing blank rows
 * 6   <blank>                  1                 12                   13       if not ignoring trailing blank rows
 *
 * rowCount = 5          for values when ignoring trailing (otherwise 7)
 * columnCount = 3       desired output
 * maxColumnCount = 3    for values
 * columnNameMap: {3=3, 2=2, 1=1, h1=1, h2=2, h3=3}
 *
 * ----- Simple with column mapping -----
 * csvSimpleColumnMapping testcase
 * columns=h1,*,h3,2
 *
 * columnIndexMap: 0, -1, 2, 1
 * rowCount = 5          for values when ignoring trailing (otherwise 7)
 * columnCount = 4       desired output
 * maxColumnCount = 3    for values
 * columnNameMap: {4=2, 3=3, 2=0, 1=1, h1=1, h3=3, h2=2}
 *
 * </pre>
 */

public class CsvHelper {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    protected static final String HTML_TABLE_START = "<table";
    protected static final int HTML_TABLE_START_LENGTH = HTML_TABLE_START.length();
    protected static final RenderMode RENDER_MODE_WIKI = RenderMode.suppress(RenderMode.F_FIRST_PARA); // for output

    protected static final String START_MACRO_TOKEN = ":MACRO:";
    protected static final String END_MACRO_TOKEN = ":END:";
    protected static final Pattern SPECIAL_MACRO_PATTERN = Pattern.compile(":MACRO:([a-zA-Z0-9\\\\-]*)\\((.*)\\):(BODY:(.*):){0,1}END:");

    protected static final String tempDoubleQuote = "\b\b"; // temporary replacement for double quotes
    protected char eolChar = '\n'; // Confluence uses this primarily
    protected boolean isAntiXssMode = true;
    protected final Map<String, Object> parameters; // optional override of macro parameters
    protected boolean escapeWiki = false;

    protected String[] values; // array of column counts for each row representing the number of elements from the values array that should be used
    protected int valuesLength; // may be reduced by removing trailing blank lines
    // protected int rowIndexStart = 0; // assume no blank rows - like for internally constructed data like attachment-table
    protected int rowCount = 0;    // number of output rows expected - often calculated from values
    protected int columnCount = 0; // number of output columns expected - ofter determined by columns parameter
    protected int maxColumnCount = 0; // max number of columns the value index for any row
    protected int firstRowNumber = 0;  // 0 based row number indicating the first non-blank row, assume no blank rows

    protected String tableId = null; // just incase user needs to control this directly instead of defaulting to parameter

    protected Map<Object, Integer> columnNameMap; // 1 based - maps column names or integers to column numbers (1-based)
    protected Map<Object, Integer> columnNameToIndexMap; // 1 based - maps column names or integers to original column index (1-based) within row

    protected List<Integer> rowColumnCountList; // 0 based list for each row representing the number of elements from the values array that are available
    protected List<Integer> rowStartIndexList; // 0 based list of value index for each row start

    List<Matcher> augmentList;
    List<Matcher> headingAugmentList;
    List<Matcher> footingAugmentList;

    protected boolean hasVariableColumnCount = false;

    protected final SubRenderer subRenderer;
    protected final TableSorter tableSorter;

    public CsvHelper(final SubRenderer subRenderer, final TableSorter tableSorter, final Map<String, Object> parameters, final String[] values) {
        this.subRenderer = subRenderer;
        this.tableSorter = tableSorter;
        this.parameters = parameters; // set to null to just use macro parameters
        this.values = values;
        valuesLength = values.length;
    }

    /**
     * Either set row and column counts directly or use initializeFromData()
     *
     * @param rowCount
     */
    public void setRowCount(int rowCount) {
        this.rowCount = rowCount; // number of data rows including headings
    }

    public int getRowCount() {
        return rowCount;
    }

    /**
     * Calcute the first data row number (0-based)
     *
     * @return
     */
    public int getFirstDataRowNumber() {
        return firstRowNumber + tableSorter.getHeading();
    }

    public int getFirstFootingRowNumber() {
        return rowCount - firstRowNumber;   // TODO
        // return rowCount - firstRowNumber + tableSorter.getFooting(); // need table sorter 2.0
    }

    /**
     * Set specific column count
     *
     * @param columnCount
     */
    public void setColumnCount(int columnCount) {
        this.columnCount = columnCount;
        maxColumnCount = columnCount;
    }

    public void setAntiXssMode(boolean isAntiXssMode) {
        this.isAntiXssMode = isAntiXssMode;
    }

    public void setEscapeWiki(boolean escapeWiki) {
        this.escapeWiki = escapeWiki;
    }

    public void setTableId(final String tableId) {
        this.tableId = tableId;
    }

    public void setEolChar(char eol) {
        eolChar = eol;
    }

    /**
     * Generate output from the list of csv values.
     */
    protected String generateOutput(final MacroInfo info) throws MacroExecutionException {

        boolean ignoreTrailingBlankRows = ScriptUtils.getBoolean("ignoretrailingblankrows", true, info);

        if (ignoreTrailingBlankRows) {
            valuesLength = getNonBlankTrailingArrayLength();
        }
        log.debug("valuesLength: {}", valuesLength);
        setRowAndColumnCountsFromData();  // after values length has been adjusted

        // setColumnNumberMap(columns);
        return constructHtml(info); // variable column count
    }

    /**
     * Construct html
     *
     * @param info
     * @return html table
     */
    protected String constructHtml(final MacroInfo info) throws MacroExecutionException {

        // long timeMarkStart = ScriptUtils.getTimeMark("start construct html", 0);
        // long timeMark = ScriptUtils.getTimeMark("start construct html", 0);

        // get values from defined parameters or macro parameters
        String columns = getString("columns", null, info); // comma separated list of column names or column numbers (1-based)
        boolean html = "html".equalsIgnoreCase(getString("output", "html", info)); // default to html unless output=wiki
        boolean escape = getBoolean("escape", false, info); // default to not escape wiki markup characters unless escape=true
        boolean showWiki = getBoolean("showwiki", false, info); // default to NOT show table unless showWiki=true
        int footing = Integer.parseInt(getString("footing", "0", info));

        setColumnNameMaps(columns); // setup mappings now
        setAugmentLists(info); // Use augments from macro parameters only

        // generate column types if not already set by user
        List<String> columnTypes = null;
        boolean needColumnTypes = null == getString("columnTypes", null, info);
        if (needColumnTypes) {
            columnTypes = new ArrayList<String>(columnCount);
            for (int i = 0; i < columnCount; i++) {
                columnTypes.add("I"); // start off thinking it is a number
            }
        }

        String startTable;
        String endTable;
        String startRow;
        String endRow;
        String startColumn;
        String endColumn;
        boolean allowExport = ScriptUtils.getBoolean("allowExport", false, info);

        if (html) {
            // TBL-7: Allow export and download of table data
            startTable = TableUtil.getStartTableHtml(allowExport, false);
            startRow = "<tr>";
            endRow = "</tr>";
            startColumn = "<th class=\"confluenceTh\">"; // first rows are heading
            endColumn = "</th>"; // first rows are heading
            // endTable = "</table>";
            log.debug("TableId:{}", tableId);
            endTable = TableUtil.getEndTableHtml(allowExport, false, info.getContent(), tableId);
            // blank = ">&nbsp;"; // consider doing this to be equivalent to excel
        } else {
            startTable = "\n ";
            endTable = " ";
            startRow = " \n ";
            endRow = "||";
            startColumn = "||";
            endColumn = " ";
            // blank = " "; // consider doing this to be equivalent to excel
        }

        int firstDataRowNumber = getFirstDataRowNumber(); // index to stop doing header row

        boolean doingHeading = true; // initialize until used once
        boolean doingFooting = false; // initialize until used once
        boolean doingAugment = headingAugmentList != null;

        StringBuilder builder = new StringBuilder();
        builder.append(startTable);

        log.debug("firstRowNumber: {}, rowCount: {}", firstRowNumber, rowCount);
        for (int rowNumber = firstRowNumber; rowNumber < rowCount; rowNumber++) { // start at first (non blank) row number

            // timeMark = ScriptUtils.getTimeMark("start handling row: " + rowNumber, timeMark);

            if (doingHeading && (rowNumber == firstDataRowNumber)) { // switch to non-heading rows - this is only done once!
                if (html) {
                    startColumn = "<td class=\"confluenceTd\">";
                    endColumn = "</td>";
                } else {
                    endRow = "|";
                    startColumn = "|";
                }
                doingHeading = false; // so we don't process this code block again
                doingAugment = doingDataAugments();
            }
            if (footing > 0) {
                doingFooting = rowNumber >= (rowCount - firstRowNumber + 1 - footing);
                if (doingFooting) {
                    doingAugment = doingFooting && (footingAugmentList != null);
                }
                log.debug("doingFooting: {}, doingAugment: {}", doingFooting, doingAugment);
            }

            // int columnCount = getColumnCount(rowNumber);
            // log.debug("columnCount: {}, for rowNumber: {}", columnCount, rowNumber);
            // log.debug("columnCount: {}", columnCount);

            for (int columnNumber = 1; columnNumber <= columnCount; columnNumber++) {

                if (columnNumber == 1) { // indicates a new row
                    builder.append(startRow);
                }

                // get the value that should be put into this spot
                String value = getValueAtPosition(rowNumber, columnNumber);

                // log.debug("rowNumber: {}, columnNumber: {}, value: " + value, rowNumber, columnNumber);

                if (value != null) { // value is suppose to be included

                    if (doingAugment) {
                        value = augmentValue(doingHeading, doingFooting, value, rowNumber, columnNumber);
                    }

                    if (html) {
                        value = ScriptUtils.handleHtmlValue(value, isAntiXssMode);
                    } else if (escape) {
                        value = ScriptUtils.ensureWikiValue(value);
                    }
                    builder.append(startColumn).append(value).append(endColumn);

                    if (needColumnTypes && !doingHeading && !doingFooting && !StringUtils.isBlank(value) && !"&nbsp;".equals(value)) {
                        int index = columnNumber - 1;
                        if (!columnTypes.get(index).equals("S")) { // not already a string
                            if (!isFloat(value)) { // not a float, then cannot be an int either
                                columnTypes.set(index, "S");
                            } else if (columnTypes.get(index).equals("I") && !isInteger(value)) {
                                columnTypes.set(index, "F");
                            }
                        }
                    }
                }
            }
            builder.append(endRow);
        }

        builder.append(endTable);

        if (!html && (showWiki)) {
            builder.append("\n{noformat}" + builder.toString() + "{noformat}");
        }
        if (columnTypes != null) {
            // ScriptUtils.log(columnTypes, "columnTypes");
            tableSorter.setColumnTypes(columnTypes);
        }
        if (!html) {  // render using WIKI renderer
            // Do not use info.getContent().toPageContext() - the link renderer is null causing link syntax to break rendering
            return removeParagraphTags(info, subRenderer.render(builder.toString(), info.getConversionContext().getPageContext(), RENDER_MODE_WIKI));
        }
        // timeMark = ScriptUtils.getTimeMark("done constructing html", timeMarkStart);
        return builder.toString();
    }

    /**
     * Should data augments be done? Opportunity for subclass to have a different calculation
     *
     * @return true if data augment processing should be done
     */
    protected boolean doingDataAugments() {
        return augmentList != null;
    }

    /**
     * Hack to cover for Confluence incompatibility - see the link below. Note the pattern is compiled (once) statically as part of CsvMacro singleton
     * https://answers.atlassian.com/questions/87730/how-to-prevent-confluence-4-x-from-inserting-paragraphs-around-table-elements-inserted-via-wiki-markup
     * http://stackoverflow.com/questions/1944213/is-it-good-to-put-p-inside-td-to-put-content-text
     *
     * @param info - used to access parameter - currently a hidden (not advertized parameter) in case we need it for support due to incompatibility
     * @param html
     * @return html with paragraphs removed from header columns
     */
    protected String removeParagraphTags(final MacroInfo info, final String html) {
        if (ScriptUtils.getBoolean("removeparagraphtags", true, info)) {
            return CsvMacro.PATTERN_REMOVE_P_TAGS.matcher(html).replaceAll("$1$2$3");
        }
        return html;
    }

    /**
     * Get column count at a specific row index respecting whether or not there are variable column counts
     *
     * @param rowNumber
     * @return count or 0
     */
    protected int getRowColumnCount(int rowNumber) {
        if (hasVariableColumnCount) {
            return (rowNumber < rowColumnCountList.size() ? rowColumnCountList.get(rowNumber) : 0);
        }
        return columnCount;
    }

    /**
     * Get row start index for requested row
     *
     * @param rowNumber
     * @return index
     */
    protected int getRowStartIndex(int rowNumber) {
        if (hasVariableColumnCount) {
            return rowNumber < rowStartIndexList.size() ? rowStartIndexList.get(rowNumber) : 0; // unlikely error if we get here and return 0
        }
        return rowNumber * maxColumnCount;  // values column count is important here
    }

    /**
     * The HTML table output has be created, now add the extra table sorter related stuff.
     *
     * @param info
     * @param html - base table html
     * @return html including all the table sorter required stuff
     */
    protected String handleTableSorterAdditions(final MacroInfo info, final String html) throws MacroExecutionException {

        StringBuilder builder = new StringBuilder();

        int tableAt = html.indexOf(HTML_TABLE_START);
        if (tableAt >= 0) {
            String localTableId = tableId;
            // Mpved below to TableUtil
            // String localTableId = tableId != null ? tableId : info.getMacroParams().getString("id", null); // default to automatically generate id
            // if (localTableId == null) {
            // localTableId = "TBL" + Long.toString(ScriptUtils.getUniqueId()); // need to have an id for the table
            // } else {
            // localTableId = GeneralUtil.htmlEncode(localTableId); // MUST be encoded!
            // }
            // FIND and update the table html element
            int endMark = html.indexOf('>', tableAt); // end mark of <table ...>
            String otherTableAttributes = html.substring(tableAt + HTML_TABLE_START_LENGTH, endMark >= 0 ? endMark : html.length());

            boolean allowExport = ScriptUtils.getBoolean("allowExport", false, info);
            boolean htmlOutput = "html".equalsIgnoreCase(getString("output", "html", info)); // default to html unless output=wiki
            if (tableAt >= 0) {
                builder.append(html.substring(0, tableAt));  // before table

                builder.append((!htmlOutput ? TableUtil.getStartTableHtml(allowExport, !htmlOutput) : "")
                        + ScriptUtils.getStandardTableHtml(info, localTableId, otherTableAttributes)); // table element
                if (endMark < html.length()) {  // not the very end
                    // get remaining data
                    TableStyleHelper tableStyleHelper = new TableStyleHelper();
                    // columnstyle will be added later
                    builder.append(tableStyleHelper.constructColumnStyleHtml(ScriptUtils.getStyles(info, "columnStyles")));
                    String tableHtmlExcludingTableBegin = html.substring(endMark + 1);
                    if (!htmlOutput) {
                        StringBuilder tempBuilder = new StringBuilder();
                        tempBuilder
                                .append(tableHtmlExcludingTableBegin.substring(0, tableHtmlExcludingTableBegin.lastIndexOf("</table>") + "</table>".length()));
                        tempBuilder.append(TableUtil.getEndTableHtml(allowExport, true, info.getContent(), tableId));
                        tempBuilder.append(tableHtmlExcludingTableBegin.substring(tableHtmlExcludingTableBegin.lastIndexOf("</table>") + "</table>".length(),
                                tableHtmlExcludingTableBegin.length()));
                        tableHtmlExcludingTableBegin = tempBuilder.toString();
                    }
                    builder.append(tableStyleHelper.modifyTableRowStyles(tableHtmlExcludingTableBegin, ScriptUtils.getStyles(info, "rowStyles"),
                            tableSorter.getHeading(), false)); // we don't do multiples for csv
                }
            }
            builder.append(tableSorter.getJavaScriptHtml(localTableId) + tableSorter.getJavaScriptForDateFunctions());
        }
        // log.debug("Output:{}", builder.toString());
        return builder.toString();
    }

    /**
     * Set up the augment lists based on parameters
     *
     * @param info
     * @throws MacroExecutionException
     */
    protected void setAugmentLists(final MacroInfo info) throws MacroExecutionException {
        augmentList = getAugmentList(info.getMacroParams().getString("augments", null));
        headingAugmentList = getAugmentList(info.getMacroParams().getString("headingaugments", null));
        footingAugmentList = getAugmentList(info.getMacroParams().getString("footingaugments", ""));
    }

    protected List<Matcher> getAugmentList(final String string) throws MacroExecutionException {
        if (string != null) {
            return getAugmentList(ScriptUtils.csvDataAsList(string, ScriptUtils.COMMA, ScriptUtils.DOUBLE_QUOTE, '\n'));
        }
        return null;
    }

    /**
     * Get a list of augment matchers corresponding to each string entry in the input list
     *
     * @param list - list of augment strings
     * @return
     */
    protected List<Matcher> getAugmentList(final List<String> list) {
        List<Matcher> matcherList = new ArrayList<Matcher>();
        Pattern pattern = Pattern.compile("%([^%]*)%");

        for (String augment : list) {
            Matcher matcher = null;

            // use % % to blank out column, use %*% or blank to preserve the value of the column
            if (!"".equals(augment.trim())) { // do nothing (preserve the column) if blank
                matcher = pattern.matcher(augment);
            }
            matcherList.add(matcher);
        }
        log.debug("augment list set with {} entries", matcherList.size());
        return matcherList;
    }

    /**
     * Given an existing column value, apply augmentation to come up with new value. The number should represent from the original column.
     *
     * @param doing heading or not
     * @param value - original value in case of * replacement
     * @param rowNumber -
     * @param columnNumber - 1-based column number (for display
     * @return new value
     */
    protected String augmentValue(boolean doingHeading, boolean doingFooting, final String value, final int rowNumber, final int columnNumber) {

        String newValue = value;  // default to original value
        // newValue = handleWikiMacroShorthand(newValue, escapeWiki);

        List<Matcher> list = (doingHeading ? headingAugmentList : doingFooting ? footingAugmentList : augmentList);

        if ((list != null) && (columnNumber <= list.size())) {
            Matcher matcher = list.get(columnNumber - 1);
            if (matcher != null) {

                StringBuffer builder = new StringBuffer(); // builder can be used in JAVA 7 ??

                matcher.reset();  // we are re-using a matcher, so it needs to be reset
                while (matcher.find()) {  // handle each replacement
                    String text = matcher.group(1).trim(); // trimmed, matched text
                    String replacement = null;

                    if (" ".equals(matcher.group(1))) {  // Allow the field to be left blank
                        replacement = "";
                    } else if ("*".equals(text)) {
                        // quote replacement gives the literal value by escaping
                        replacement = Matcher.quoteReplacement(getDefaultReplacement(value));
                    } else if ("#".equals(text)) {
                        replacement = (doingHeading || doingFooting) ? "" : Integer.toString(rowNumber - getFirstDataRowNumber() + 1);
                    } else if ("!".equals(text)) {  // help for wiki parameters: use %!% for |
                        replacement = "|";
                    } else if ("[".equals(text)) {  // help for wiki parameters: use %[% for {
                        replacement = "{";
                    } else if ("]".equals(text)) {  // help for wiki parameters: use %]% for }
                        replacement = "}";
                    } else if (!text.equals("")) {
                        // try a lookup first
                        // log.debug("text: {}, size: {}", text, text.length());
                        Integer number = columnNameToIndexMap.get(text.toLowerCase());
                        if (number == null) {
                            if (StringUtils.isNumeric(text)) {
                                number = Integer.parseInt(text);
                                columnNameToIndexMap.put(text, number); // add this specific column to number mapping for next time
                            }
                        }
                        if ((number != null) && (number > 0)) {
                            if (number <= getRowColumnCount(rowNumber)) {
                                replacement = Matcher.quoteReplacement(values[getRowStartIndex(rowNumber) + number - 1]);
                            } else {
                                replacement = "";
                            }
                        }
                    }
                    // Not needed yet for customized replacement values
                    // if (replacement == null) { // no replacement found yet
                    // replacement = Matcher.quoteReplacement(customizeAugmentValue(text, value));
                    // }

                    // og.debug("text (not trimmed): {}, replacement: {}", matcher.group(1), replacement);
                    if (replacement != null) {
                        matcher.appendReplacement(builder, replacement);  // quote replacement used above
                    }
                }
                matcher.appendTail(builder);
                newValue = builder.toString();

            } else {
                // log.debug("matcher is null");
                newValue = getDefaultReplacement(value);
            }
            // newValue = handleWikiMacroShorthand(newValue);
        } else {
            // log.debug("no augments");
            newValue = getDefaultReplacement(value);
        }
        newValue = convertWikiMacroShorthand(newValue, escapeWiki);
        // log.debug("new value: {}", newValue);
        return newValue;
    }

    /**
     * Opportunity for subclass to add additional handling of default value
     *
     * @param value
     * @return
     */
    protected String getDefaultReplacement(final String value) {
        return value;
    }

    /**
     * Opportunity for subclass to add additional variable substitution. Currently not used.
     *
     * @param textvalue
     * @return
     */
    // protected String customizeAugmentValue(final String text, final String value) {
    // return null;
    // }

    /**
     * See TBL-342. Convert wiki macro shorthand - to real wiki markup primarily for those using wiki markup. Convert:
     * MACRO:json-table(path=...;columns=1,2,3):BODY:%1% -> {json-table:path=...|columns=1,2,3}%1%{json-table}
     *
     * @param value
     * @return converted value
     */
    protected String convertWikiMacroShorthand(final String value, boolean escapeWiki) {

        log.debug("convert size: {}, value: {}", value.length(), value);
        Matcher matcher = SPECIAL_MACRO_PATTERN.matcher(value);
        if (matcher.find()) {
            StringBuilder builder = new StringBuilder();

            int startToken = matcher.start();
            int endToken = ScriptUtils.findMatchedEndTableIndex(value, startToken);

            // log.debug("startToken: {}, endToken: {}", startToken, endToken);

            if (startToken > 0) {  // something before the start
                String segment = value.substring(0, startToken);
                builder.append(escapeWiki ? ScriptUtils.ensureWikiValue(segment) : segment);
            } else if (endToken >= value.length()) { // exact match
                // 1: macro name
                // 2: macro parameters
                // 3: body markup segment (optional)
                // 4: body
                // log.debug("1: " + matcher.group(1) + ", 2: " + matcher.group(2) + ", 3: " + matcher.group(3) + ", 4: " + matcher.group(4));

                boolean hasBody = (matcher.group(3) != null);
                String replacement = "{$1:$2}" + (hasBody ? "$4{$1}" : "");
                builder.append(matcher.replaceFirst(replacement).replaceAll(";", "|"));
            }

            if (endToken < value.length()) {  // handle a subset now
                builder.append(convertWikiMacroShorthand(value.substring(startToken, endToken + END_MACRO_TOKEN.length()), escapeWiki));
                builder.append(escapeWiki ? ScriptUtils.ensureWikiValue(value) : value); // add the tail
            } else if (startToken > 0) {  // not handled by exact match
                builder.append(convertWikiMacroShorthand(value.substring(startToken), escapeWiki));
            }

            String result = builder.toString();
            log.debug("result after macro replacement: {}", result);
            return result;
        } else {
            return value;
        }
    }

    /**
     * NOTE: This simplier form didn't work properly for escaping wiki markup. Which is why the more detailed alternative was developed. See TBL-342. Wiki macro
     * shorthand - handle special replacement primarily for those using wiki markup. Convert: MACRO:json-table(path=...;columns=1,2,3):BODY:%1% ->
     * {json-table:path=...|columns=1,2,3}%1%{json-table}
     *
     * @param value
     * @return converted value
     */
    protected String ALTERNATEconvertWikiMacroShorthand(final String value, boolean escape) {

        Matcher matcher = SPECIAL_MACRO_PATTERN.matcher(value);

        StringBuffer builder = new StringBuffer(); // builder can be used in JAVA 7 ??
        while (matcher.find()) {
            boolean hasBody = (matcher.group(3) != null);
            String replacement = "{$1:$2}" + (hasBody ? "$4{$1}" : "");
            replacement = replacement.replaceAll(";", "|");
            matcher.appendReplacement(builder, replacement);
        }
        matcher.appendTail(builder);
        String result = builder.toString();
        log.debug("result after macro replacement: {}", result);
        return result;
    }

    /**
     * Get value for the specific position handling mappings and out of range requests
     *
     * @param rowNumber - 0 based row number
     * @param columnNumber - 1 based column number of requested columns, if 0 then return empty string
     * @return
     */
    protected String getValueAtPosition(final int rowNumber, final int columnNumber) {

        // log.debug("rowNumber: {}, columnNumber: {}", rowNumber, columnNumber);
        if ((columnNumber > 0) && (rowNumber < rowCount)) {

            int offset = getMappedColumnOffset(columnNumber); // index to add to row beginning index

            if ((offset >= 0) && (offset < getRowColumnCount(rowNumber))) {
                int index = offset + getRowStartIndex(rowNumber);
                // log.debug("rowStartIndex: {}, index: {}", getRowStartIndex(rowNumber), index);
                if (index < valuesLength) {
                    return values[index];
                }
            } else {
                return "";
            }
            // else {
            // log.debug("index: {}", index);
            // }
        }
        return null;
    }

    /**
     * For a 1-based column number, get the 0-based column index in the values array
     *
     * @param columnNumber
     * @return column index within row
     */
    protected int getMappedColumnOffset(int columnNumber) {
        if (columnNameMap != null) {
            Integer mapped = columnNameMap.get(columnNumber);
            return (mapped == null ? 0 : (mapped - 1));
        }
        return columnNumber - 1;
    }

    /**
     * Set row, column, maxColumn counts from the values data
     */
    protected void setRowAndColumnCountsFromData() {

        hasVariableColumnCount = true;

        rowColumnCountList = new ArrayList<Integer>();
        rowStartIndexList = new ArrayList<Integer>();

        boolean isNotBlank = false;
        boolean isFirstRowNumberSet = false;

        int rowColumnCount = 0;
        int rowStartIndex = 0;

        // loop through the values picking out the rows and column counts
        for (int i = 0; i < valuesLength; i++) {
            rowColumnCount++;

            int lastIndex = values[i].length() - 1; // index of last char of string

            // when this column is the end of input or is the last column in this row, update row index
            if ((i == valuesLength - 1) || (lastIndex >= 0) && (values[i].charAt(lastIndex) == eolChar)) {
                // newRow = true; // this column is the last column in this row, so indicate new row
                rowColumnCountList.add(rowColumnCount); // set the count for this row
                rowStartIndexList.add(rowStartIndex); // index of valuesArray of the start of row

                if (rowColumnCount > maxColumnCount) {
                    maxColumnCount = rowColumnCount;
                }
                rowColumnCount = 0; // reset for next row
                rowStartIndex = i + 1; // reset for next row
            }

            values[i] = values[i].trim(); // trim the values data to loose the EOL_CHAR in particular
            if (!isFirstRowNumberSet) {
                isNotBlank = isNotBlank || !values[i].equals("");
                if (isNotBlank) { // found a non blank entry
                    firstRowNumber = rowColumnCountList.size() + ((rowColumnCount == 0) ? -1 : 0); // one more if in middle of row
                    isFirstRowNumberSet = true;
                    log.debug("firstRowNumber: {}", firstRowNumber);
                }
            }
        }

        rowCount = rowColumnCountList.size();

        if (log.isDebugEnabled()) {
            log.debug("rowCount: {}, maxColumnCount: {}", rowCount, maxColumnCount);
            // ScriptUtils.log(rowColumnCountList.toArray(), "rowColumnCountList");
            // ScriptUtils.log(rowStartIndexList.toArray(), "rowStartIndexList");
        }
    }

    /**
     * Find the length on an array if trailing blank entries are ignored
     *
     * @param values - any string array
     * @return - length of the array if trailing blank entries are ignored
     */
    protected int getNonBlankTrailingArrayLength() {
        int lastIndex = values.length - 1;
        while ((lastIndex >= 0) && values[lastIndex].trim().equals("")) {
            lastIndex--;
        }
        return lastIndex + 1;
    }

    /**
     * Set up the column name maps - map heading name and column number strings to int column numbers. Used in later lookups.
     */
    protected void setColumnNameMaps(final String columns) {

        int[] columnIndexMap = getColumnIndexMap(columns);  // could be null
        columnNameMap = new HashMap<Object, Integer>(); // must not be created before getColumnIndex map due to lookup processing
        columnNameToIndexMap = new HashMap<Object, Integer>();

        // Add all the numeric mappings
        for (int i = 1; i <= columnCount; i++) {
            columnNameMap.put(i, columnIndexMap == null ? i : getColumnNumber(columnIndexMap, i));
        }

        // loop through the first (header) row to gather heading names
        int rowColumnCount = getRowColumnCount(firstRowNumber); // first non-blank data row
        for (int i = 0; i < rowColumnCount; i++) {
            int columnNumber = i + 1;  // 1-based column number
            // final String value = values[getRowStartIndex(firstRowNumber) + i]; // heading value
            String value = values[getRowStartIndex(firstRowNumber) + i]; // heading value

            if (!StringUtils.isBlank(value)) {
                value = value.trim().toLowerCase();
                int number = getColumnNumber(columnIndexMap, columnNumber); // redirects to rquested column number
                if (number <= 0) {
                    number = columnNumber;
                }
                columnNameMap.put(value, number);
                columnNameToIndexMap.put(value, columnNumber);
            }
        }
        log.debug("columnNameMap: {}", columnNameMap);
        log.debug("columnNameToIndexMap (headings): {}", columnNameToIndexMap); // used in augment processing
    }

    /**
     * Get 1 based column number mapping
     *
     * @param columnIndexMap
     * @param number
     * @return
     */
    protected int getColumnNumber(final int[] columnIndexMap, final int number) {
        return (columnIndexMap == null) || (number > columnIndexMap.length) ? 0 : columnIndexMap[number - 1] + 1;
    }

    /**
     * Get 0 based mapping of columns requested (index) to column index within values row. Map to -1 if requested column is not found. Also reset columnCount.
     *
     * @param columns - comma separated list of column names or numbers or mix
     * @return
     */
    protected int[] getColumnIndexMap(final String columns) {

        int[] indexMap = null;
        if (StringUtils.isBlank(columns)) {
            columnCount = maxColumnCount;
        } else {
            String[] columnList = columns.split(",");
            columnCount = columnList.length;
            // temporary until completed due to how lookupColumnNumber works initially versus after this is completed
            indexMap = new int[columnCount];

            for (int i = 0; i < indexMap.length; i++) {
                String column = columnList[i].trim();
                indexMap[i] = lookupColumnNumber(column) - 1; // -1 for not found
                // indexMap[i] = index < 0 ? Integer.MAX_VALUE : index;
            }
            ScriptUtils.log(indexMap, "columnIndexMap");
        }
        log.debug("columnCount: {}", columnCount);
        return indexMap;
    }

    /**
     * Lookup column number for a column. 1 based, 0 indicates column not found or blank. Use only the name field.
     *
     * @param column - lowercased and trimmed column name
     * @return 1 based column number or 0 if not found
     */
    protected int lookupColumnNumber(String column) {

        Integer columnNumber = null;
        column = column.trim().toLowerCase();  // normalize to all lower case
        if (!column.equals("")) {
            if (columnNameMap != null) {
                columnNumber = columnNameMap.get(column);
            } else {
                if (StringUtils.isNumeric(column)) {
                    columnNumber = Integer.parseInt(column);
                } else { // not a number, look up by name through the heading row
                    for (int i = 0; i < getRowColumnCount(firstRowNumber); i++) {
                        String value = values[getRowStartIndex(firstRowNumber) + i];
                        if (value != null) {
                            value = value.trim().toLowerCase(); // normalize the value
                        }
                        // log.debug("column: {}, normalize value: {}", column, value);
                        if (column.equals(value)) {
                            columnNumber = i + 1;
                            break; // done looking
                        }
                    }
                }
            }
            if (columnNumber != null) {
                // log.debug("columnNumber found: {}", columnNumber);
                return columnNumber;
            }
        }
        return (columnNumber == null ? 0 : columnNumber); // 0 if not found
    }

    /**
     * Get quote parameter
     *
     * @param info
     * @return quote or exception
     * @throws MacroExecutionException
     */
    protected char getQuoteFromParameter(final MacroInfo info) throws MacroExecutionException {
        String quote = getString("quote", "\"", info).trim(); // default to double quote
        if (quote.equalsIgnoreCase("single")) {
            quote = "'";
        } else if (quote.equalsIgnoreCase("double")) {
            quote = "\"";
        } else if (quote.length() != 1) {
            throw new MacroExecutionException("Quote parameter must be a single character.");
        }
        return quote.charAt(0);
    }

    /**
     * remove quotes - convert double quotes to single, hack - works in most cases :{ - maybe fix later
     */
    protected String handleQuotes(String string, final String quote, final Pattern quotePattern, final Pattern doubleQuotePattern,
            final Pattern tempDoubleQuotePattern) {
        // hack double quotes to something else
        Matcher matcher = doubleQuotePattern.matcher(string);

        boolean doubleQuoteFound = matcher.find();

        if (doubleQuoteFound) {
            string = matcher.replaceAll(tempDoubleQuote);
        }

        // remove single quotes
        string = quotePattern.matcher(string).replaceAll("");

        // turn double quotes to single quotes
        if (doubleQuoteFound) {
            string = tempDoubleQuotePattern.matcher(string).replaceAll(quote);
        }

        return string;
    }

    /**
     * Get string parameter either from parameters if defined or macro
     *
     * @param string - parameter name
     * @param defaultValue - default if not available
     * @param info - macro info
     * @param parameters - optional parameter map, use if not null
     */
    protected String getString(final String string, final String defaultValue, final MacroInfo info) {
        if ((parameters != null) && parameters.containsKey(string)) {
            Object value = parameters.get(string);
            if (value instanceof String) {
                return (String) value;
            }
            return defaultValue;
        } else {
            return info.getMacroParams().getString(string, defaultValue);
        }
    }

    /**
     * Get boolean parameter either from parameters if defined or macro parameters
     *
     * @param string - parameter name
     * @param defaultValue - default if not available
     * @param info - macro info
     * @param parameters - optional parameter map, use if not null
     * @return
     */
    protected boolean getBoolean(final String string, boolean defaultValue, final MacroInfo info) {
        if ((parameters != null) && parameters.containsKey(string)) {
            Object value = parameters.get(string);
            if (value instanceof Boolean) {
                return (Boolean) value;
            }
            return defaultValue;
        } else {
            return info.getMacroParams().getBoolean(string, defaultValue);
        }
    }

    /**
     * is string a float?
     */
    protected boolean isFloat(final String data) {
        try {
            Float.parseFloat(data);
        } catch (NumberFormatException exception) {
            return false;
        }
        return true;
    }

    /**
     * is string a boolean
     */
    protected boolean isInteger(final String data) {
        try {
            Integer.parseInt(data);
        } catch (NumberFormatException exception) {
            return false;
        }
        return true;
    }
}
