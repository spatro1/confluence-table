/*
 * Copyright (c) 2005, 2013 Bob Swift Software, LLC.
 * All rights reserved.
 *
 * This software is licensed under the provisions of the "Standard EULA" from the
 * "Atlassian Marketplace Terms of Use" as a "Marketplace Product". Reference is:
 * http://www.atlassian.com/licensing/marketplace/termsofuse.
 * The this case, the "Publisher" is Bob Swift Software, LLC
 * and the "Marketplace Product" is Table Plugin for Confluence.
 *
 * See the LICENSE file for more details.
 */

package org.swift.confluence.table;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.swift.confluence.macrosecurity.utils.MacroSecurityUtils;
import org.swift.confluence.macroutil.MacroInfo;
import org.swift.confluence.scriptutil.Constants;
import org.swift.confluence.scriptutil.DateHelper;
import org.swift.confluence.scriptutil.LicensedScriptMacro;
import org.swift.confluence.scriptutil.ScriptUtils;
import org.swift.confluence.scriptutil.TableHelper;
import org.swift.confluence.tablesorter.TableSorter;
import org.swift.confluence.tablesorter.TableUtil;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.core.FormatSettingsManager;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.templates.PageTemplateManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.SubRenderer;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.upm.api.license.PluginLicenseManager;
import com.atlassian.webresource.api.assembler.PageBuilderService;

/**
 * This macro creates a csv table from attachment data based on various filters
 */
public class AttachmentTableMacro extends LicensedScriptMacro {

    protected final boolean EXEMPT_DEVELOPER_LICENSE = true;

    final protected PageBuilderService pageBuilderService; // for js resource
    final protected SubRenderer subRenderer;
    final protected BandanaManager bandanaManager;
    final protected UserAccessor userAccessor;
    final protected DateHelper dateHelper;
    final protected I18nResolver i18n;

    protected static final RenderMode RENDER_MODE_WIKI = RenderMode.suppress(RenderMode.F_FIRST_PARA); // for output
    protected static final String DEFAULT_COLUMNS_SINGLE = "file,size,creator,created,comment";
    protected static final String DEFAULT_COLUMNS = "page," + DEFAULT_COLUMNS_SINGLE;
    protected static final int DEFAULT_LIMIT = 1000; // number of attachments limit, should match default for macro browser

    /**
     * Constructor
     */
    protected AttachmentTableMacro(final SettingsManager settingsManager, final FormatSettingsManager formatSettingsManager,
            final BootstrapManager bootstrapManager, final PageTemplateManager templateManager, final SpaceManager spaceManager, final PageManager pageManager,
            final PermissionManager permissionManager, final AttachmentManager attachmentManager, final LocaleManager localeManager,
            final I18NBeanFactory i18NBeanFactory, final PluginLicenseManager licenseManager, final PageBuilderService pageBuilderService,
            final XhtmlContent xhtmlContent, final SubRenderer subRenderer, final BandanaManager bandanaManager, final UserAccessor userAccessor,
            final I18nResolver i18n) {
        super(settingsManager, bootstrapManager, templateManager, spaceManager, pageManager, permissionManager, attachmentManager, localeManager,
                i18NBeanFactory, licenseManager, xhtmlContent);
        this.pageBuilderService = pageBuilderService;
        this.subRenderer = subRenderer;
        this.bandanaManager = bandanaManager;
        this.userAccessor = userAccessor;
        this.i18n = i18n;
        dateHelper = new DateHelper(settingsManager, formatSettingsManager, userAccessor);
    }

    /**
     * Should we allow use for Confluence developer license even if no valid plugin license?
     *
     * @return true to allow use or false to report normal errors for invalid licenses
     */
    @Override
    protected boolean exemptDeveloperLicense() {
        return EXEMPT_DEVELOPER_LICENSE;
    }

    @Override
    protected String getExtension() {
        return ".csv";
    }

    @Override
    protected String getMacroName() {
        return "attachment-table";
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

    @Override
    public String execute(final Map<String, String> params, final String body, final ConversionContext conversionContext) throws MacroExecutionException {
        // log.debug("link renderer from conversion context: " + conversionContext.getPageContext().getLinkRenderer());
        return super.execute(params, body, conversionContext);
    }

    /**
     * Primary processing
     */
    @Override
    protected String execute(MacroInfo info) throws MacroExecutionException {
        validateLicense(licenseManager);

        if (log.isDebugEnabled()) {
            ScriptUtils.logParams(info);
        }
        try {
            // boolean areAugmentsDefined = info.getMacroParams().getString("augments", null) != null;
            boolean isSinglePage = !StringUtils.isBlank(getPageParameter(info));
            String columns = info.getMacroParams().getString("columns", isSinglePage ? DEFAULT_COLUMNS_SINGLE : DEFAULT_COLUMNS);
            if (columns.equalsIgnoreCase(Constants.ALL)) {
                columns = AttachmentTableHelper.ALL_COLUMNS;
            }
            String[] columnListToShow = columns.split(",");
            // String[] columnList = areAugmentsDefined ? AttachmentTableHelper.ALL_COLUMN_ARRAY : columnListToShow;

            // long timeMark = ScriptUtils.getTimeMark("get attachment list", 0);

            AttachmentTableHelper attachmentTableHelper = new AttachmentTableHelper(settingsManager, i18n, permissionManager, userAccessor, dateHelper,
                    columnListToShow, info.getMacroParams().getString("dateFormat", ""));
            attachmentTableHelper.setAttachmentPattern(getPattern(info.getMacroParams().getString("attachmentregex", null)));
            attachmentTableHelper.setCommentPattern(getPattern(info.getMacroParams().getString("commentregex", null)));
            attachmentTableHelper.setLabelPattern(getPattern(info.getMacroParams().getString("labelregex", null)));
            attachmentTableHelper.setLabelMatchOption(info.getMacroParams().getString("labelmatchoption", null));

            List<Attachment> attachmentList = generateAttachmentList(info, attachmentTableHelper);
            // timeMark = ScriptUtils.getTimeMark("after attachment list", timeMark);

            int rowCount = attachmentList.size() + 1; // +1 for heading
            if (rowCount > 1) {  // there is at least 1 attachment

                final String[] values = attachmentTableHelper.generateAttachmentValues(attachmentList);
                attachmentList = null; // not needed anymore, free for garbage collection

                TableSorter tableSorter = TableUtil.setupTableSorter(new TableHelper().getTableProperties(info), pageBuilderService);
                tableSorter = TableUtil.updateSortTip(tableSorter, getStringOrTranlation(tableSorter.getSortTip(), "@org.swift.confluence.table.sortTip"));
                String columnTypes = info.getMacroParams().getString("columntypes", null);
                if (columnTypes == null) {
                    tableSorter.setColumnTypes(attachmentTableHelper.generateColumnTypes());
                }
                // else let tableSorter set it from macro parameters if defined
                // timeMark = ScriptUtils.getTimeMark("after setting column types", timeMark);

                // use these parameters in place of some of the macro parameters
                Map<String, Object> parameters = new HashMap<String, Object>();
                parameters.put("output", "wiki");
                // if (areAugmentsDefined) {
                // parameters.put("columns", "");
                // }
                parameters.put("columns", null);
                parameters.put("columnTypes", ""); // blank means nothing leaving the current table sorter column types
                // let other stuff default or use the macro parameters directly

                CsvHelper csvHelper = new CsvHelper(subRenderer, tableSorter, parameters, values);
                csvHelper.setRowCount(rowCount);
                csvHelper.setColumnCount(columnListToShow.length);
                csvHelper.setAntiXssMode(isAntiXssMode(info));
                String tableId = TableUtil.generateTableId(info.getMacroParams().getString("id", null));
                csvHelper.setTableId(tableId);
                StringBuilder builder = new StringBuilder();
                builder.append(csvHelper.handleTableSorterAdditions(info, csvHelper.constructHtml(info)));
                // TBL-55: Columnstyles shall be applied later, after sticky headers loaded
                // builder.append(TableUtil.columnStyleHtml(new TableStyleHelper().constructColumnStyleHtml(ScriptUtils.getStyles(info, "columnStyles")),
                // tableId));
                // log.debug("table:{}", builder.toString());
                return builder.toString();

                // timeMark = ScriptUtils.getTimeMark("before constructing html", timeMark);
                // String html = csvHelper.constructHtml(info);
                // timeMark = ScriptUtils.getTimeMark("after constructing html", timeMark);
                // return csvHelper.handleTableSorterAdditions(info, html);
            } else {
                return info.getMacroParams().getString("textForNone", "");
            }
        } catch (MacroExecutionException exception) {
            if (log.isDebugEnabled()) {
                exception.printStackTrace();
            }
            throw exception; // rethrow
        } catch (Exception exception) {
            log.error(info.getMacroBody(), exception); // log unexpected exception for problem determination
            throw new MacroExecutionException("Unexpected program error: " + exception.toString());
        }
    }

    /**
     * Generate a list of attachments matching criteria
     */
    protected List<Attachment> generateAttachmentList(final MacroInfo info, final AttachmentTableHelper attachmentTableHelper) throws MacroExecutionException {

        // long timeMark = ScriptUtils.getTimeMark("start attachment list", 0);
        List<Attachment> list = new ArrayList<Attachment>();

        String spaceRegex = info.getMacroParams().getString("spaceregex", null);
        String pageRegex = info.getMacroParams().getString("pageregex", null); // logic later will default to all pages
        String pageLabelRegex = info.getMacroParams().getString("pagelabelregex", null); // logic later will default to all pages

        final boolean includePersonalSpaces = ScriptUtils.getBoolean("includePersonalSpaces", false, info);
        int limit = ScriptUtils.getInteger("limit", DEFAULT_LIMIT, info);
        if (limit < 0) {
            limit = Integer.MAX_VALUE;  // no limit
        }
        log.debug("space regex: {}, pageRegex: {}", spaceRegex, pageRegex);

        String pageString = getPageParameter(info);  // page take precedence over a page pattern

        if (!StringUtils.isBlank(pageString)) { // single page
            boolean onCurrentPage = pageString.trim().equalsIgnoreCase(Constants.SELF); // prevent preview problems, browser or main preview

            // handles special @self special case to preview preview problems TBL-273
            Page singlePage = onCurrentPage ? getPageWithSpecialValueHandling(pageString, info.getConversionContext().getPageContext(), null, true)
                    : getPage(pageString, info.getConversionContext().getPageContext()); // checks permission and handles special values
            log.debug("single page: {}", singlePage);
            attachmentTableHelper.processForPage(info, list, singlePage, null, null, limit);

        } else { // pattern search

            Pattern pagePattern = getPattern(pageRegex);
            Pattern pageLabelPattern = getPattern(pageLabelRegex);

            List<Space> spaceList = new ArrayList<Space>();

            if (Constants.SELF.equalsIgnoreCase(spaceRegex)) {
                log.debug("self space: {}", info.getConversionContext().getSpaceKey()); // this handles preview case as well
                Space space = spaceManager.getSpace(info.getConversionContext().getSpaceKey());
                if (space != null) {
                    spaceList = new ArrayList<Space>();
                    spaceList.add(space);
                } else {
                    throw new MacroExecutionException(i18n.getText(i18nKey + ".error.space-not-available-for-self"));
                }
            } else {  // generate a space list from the regex
                if (spaceRegex.equalsIgnoreCase(Constants.ALL)) {
                    spaceRegex = ".*"; // ease of use for people who want to subset by page name
                    log.debug("@all processing");
                }
                spaceList = generateSpaceList(info, getPattern(spaceRegex), includePersonalSpaces);
            }
            int spaceCounter = 0;
            int pageCounter = 0;
            for (Space space : spaceList) {
                spaceCounter++;
                if (Constants.SELF.equalsIgnoreCase(pageRegex)) { // unusual case, matching on the title of the current page
                    Page page = pageManager.getPage(space.getKey(), info.getContent().getTitle());
                    attachmentTableHelper.processForPage(info, list, page, null, pageLabelPattern, limit);
                } else {
                    for (Page page : getPageListForSpace(space)) {
                        pageCounter++;
                        attachmentTableHelper.processForPage(info, list, page, pagePattern, pageLabelPattern, limit);
                        if (list.size() >= limit) {
                            break;
                        }
                    }
                }
                if (list.size() >= limit) {
                    break;
                }
            }
            log.debug("space counter: {}, pageCounter: {}", spaceCounter, pageCounter);
        }
        log.debug("attachmentList size: {}, limit: {}", list.size(), limit);
        // timeMark = ScriptUtils.getTimeMark("finish attachment list", timeMark);

        return list;
    }

    /**
     * Get the page parameter with defaulting to self if blank and spaceregex is also blank - results in single page processing
     *
     * @param info
     * @return page parameter as specified by user or modified to SELF if appropriate
     */
    protected String getPageParameter(final MacroInfo info) {
        String page = info.getMacroParams().getString("page", null);  // page take precedence over a page pattern
        if (StringUtils.isBlank(page) && StringUtils.isBlank(info.getMacroParams().getString("spaceregex", null))) {
            page = Constants.SELF;
        }
        log.debug("modified page parameter: {}", page);
        return page;
    }

    /**
     * Generate a space list (sorted by key) from the space regex either matching on key or name
     *
     * @param pattern
     * @param includePersonalSpaces
     * @return list of spaces (not null)
     */
    protected List<Space> generateSpaceList(final MacroInfo info, final Pattern pattern, boolean includePersonalSpaces) throws MacroExecutionException {
        List<Space> list = new ArrayList<Space>();
        if (pattern != null) {
            for (Space space : spaceManager.getAllSpaces()) {
                if (space.isGlobal() || includePersonalSpaces) { // filter out personal spaces unless requested otherwise
                    if (permissionManager.hasPermission(info.getCurrentUser(), Permission.VIEW, space)) {
                        Matcher matcher = pattern.matcher(space.getKey());
                        if (matcher.matches()) {  // match on key
                            list.add(space);
                        } else {
                            matcher = pattern.matcher(space.getName());
                            if (matcher.matches()) { // match on name
                                list.add(space);
                                // log.debug("include space: {}", space.getKey());
                            }
                        }
                    }
                }
            }
        }

        Collections.sort(list, new Comparator<Space>() {  // sort by key
            @Override
            public int compare(final Space s1, final Space s2) {
                return s1.getKey().compareTo(s2.getKey());
            }
        });
        return list;
    }

    /**
     * Get a list of pages for a specific space (ordered by title)
     *
     * @param space
     * @return
     */
    protected List<Page> getPageListForSpace(final Space space) {
        List<Page> list = pageManager.getPages(space, true);  // true for current pages only
        Collections.sort(list, new Comparator<Page>() {  // sort by title
            @Override
            public int compare(final Page p1, final Page p2) {
                return p1.getTitle().compareTo(p2.getTitle());
            }
        });
        // if (log.isDebugEnabled()) {
        // log.debug("page list for space: {}", space.getKey());
        // for (Page page : list) {
        // log.debug("page: {}", page.getTitle());
        // }
        // }
        return list;
    }

    /**
     * Safely get a pattern from the input regex string
     *
     * @param regex
     * @return
     * @throws MacroExecutionException
     */
    protected Pattern getPattern(final String regex) throws MacroExecutionException {
        try {
            return (!StringUtils.isBlank(regex) ? Pattern.compile(regex) : null);
        } catch (Exception exception) {
            throw new MacroExecutionException(i18n.getText(i18nKey + ".error.invalid-regex") + ": " + regex + ". " + exception.getMessage());
        }
    }

    /**
     * Determine if antiXss mode is on or not
     *
     * @param info
     * @return
     */
    protected boolean isAntiXssMode(final MacroInfo info) throws MacroExecutionException {
        boolean isAntiXssMode = settingsManager.getGlobalSettings().isAntiXssMode();

        if (ScriptUtils.getBoolean("disableantixss", false, info)) { // if set to true and authorized, then reset
            if (MacroSecurityUtils.securityCheck(bandanaManager, userAccessor, getMacroName(), info, "disableAntiXss", null)) {
                isAntiXssMode = false;
            }
        }
        return isAntiXssMode;
    }
}
