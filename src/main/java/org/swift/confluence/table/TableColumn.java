/*
 * Copyright (c) 2005, 2013 Bob Swift Software, LLC.
 * All rights reserved.
 *
 * This software is licensed under the provisions of the "Standard EULA" from the
 * "Atlassian Marketplace Terms of Use" as a "Marketplace Product". Reference is:
 * http://www.atlassian.com/licensing/marketplace/termsofuse.
 * The this case, the "Publisher" is Bob Swift Software, LLC
 * and the "Marketplace Product" is Table Plugin for Confluence.
 *
 * See the LICENSE file for more details.
 */

package org.swift.confluence.table;

import com.atlassian.sal.api.message.I18nResolver;

public class TableColumn {

    // protected final Logger log = LoggerFactory.getLogger(getClass());

    final protected String name; // used in columns to uniquely identify the column of interest
    final protected String columnType; // sort column type

    TableColumn(final String name, final String columnType) {
        this.name = name;
        this.columnType = columnType;
    }

    public String getName() {
        return name;
    }

    public String getHeading(final I18nResolver i18n) {
        String heading = i18n.getText("org.swift.confluence.table.attachment-table.heading." + name);
        if (heading.contains("org.swift")) {
            heading = ""; // if the heading cannot be found, revert to a blank heading
        }
        // log.debug("heading name: {}, heading: {}", name, heading);
        return heading;
    }

    public String getColumnType() {
        return columnType;
    }
}
