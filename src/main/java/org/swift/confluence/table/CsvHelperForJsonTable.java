/*
 * Copyright (c) 2014 Appfire Technologies, Inc.
 * All rights reserved.
 *
 * This software is licensed under the provisions of the "Standard EULA" from the
 * "Atlassian Marketplace Terms of Use" as a "Marketplace Product". Reference is:
 * http://www.atlassian.com/licensing/marketplace/termsofuse.
 * The this case, the "Publisher" is Appfire Technologies, Inc
 * and the "Marketplace Product" is Table Plugin for Confluence.
 *
 * See the LICENSE file for more details.
 */

package org.swift.confluence.table;

import java.util.Map;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.swift.confluence.macroutil.MacroInfo;
import org.swift.confluence.scriptutil.ScriptUtils;
import org.swift.confluence.tablesorter.TableSorter;

import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.renderer.v2.SubRenderer;

public class CsvHelperForJsonTable extends CsvHelper {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    protected final MacroInfo info;
    protected final boolean isWiki;
    protected final boolean stripQualifiers;
    protected final boolean capitalize;
    protected final boolean disableAntiXss;

    protected boolean escapeWiki;  // escape wiki only within non-JSON elements

    protected String sortPaths;
    protected String fieldOrderRegexPatterns;

    public CsvHelperForJsonTable(final MacroInfo info, final SubRenderer subRenderer, final TableSorter tableSorter, final Map<String, Object> parameters,
            final String[] values) {
        super(subRenderer, tableSorter, parameters, values);
        this.info = info;
        isWiki = "wiki".equalsIgnoreCase(getString("output", "html", info));
        stripQualifiers = getBoolean("stripqualifiers", false, info); // strip path qualifiers in heading rows
        capitalize = getBoolean("capitalize", true, info); // capitalize first char in header rows by default
        disableAntiXss = getBoolean("disableantixss", false, info); // capitalize first char in header rows by default
    }

    public void setSortPaths(String sortPaths) {
        this.sortPaths = sortPaths;
    }

    public void setFieldOrderRegexPatterns(String fieldOrderRegexPatterns) {
        this.fieldOrderRegexPatterns = fieldOrderRegexPatterns;
    }

    @Override
    public void setEscapeWiki(boolean escapeWiki) {
        this.escapeWiki = escapeWiki;
    }

    /**
     * Should data augments be done? Need augments for automatic json expansion when doing wiki markup
     * 
     * @return
     */
    @Override
    protected boolean doingDataAugments() {
        return (augmentList != null) || isWiki;  // isWiki means we are going to default to converting json stuff to json-table macros
    }

    /**
     * Generate output from the list of csv values. Handle special case of HTML with simple elements not being able to set a blank heading row
     */
    @Override
    protected String generateOutput(final MacroInfo info) throws MacroExecutionException {
        return isWiki ? super.generateOutput(info) : super.generateOutput(info).replace(">&amp;nbsp;</th>", ">&nbsp;</th");
    }

    /**
     * For wiki and elements that are JSON, convert to another json-table macro to get a reasonable expansion
     * 
     * @param value
     * @return
     */
    protected String ALTERNATEgetDefaultReplacement(final String value) {
        String result = value;
        String trimmedValue = value.trim();
        // log.debug("default replacement - isWiki: {}, value: {}", isWiki, value);

        if (isWiki && JsonUtils.isJson(trimmedValue)) {

            boolean stripQualifiers = getBoolean("stripqualifiers", false, info); // strip path qualifiers in heading rows
            boolean capitalize = getBoolean("capitalize", true, info); // capitalize first char in header rows by default

            StringBuilder builder = new StringBuilder();
            builder.append(":MACRO:json-table(output=wiki") //
                    .append(";stripQualifiers=").append(stripQualifiers ? ScriptUtils.TRUE : ScriptUtils.FALSE) //
                    .append(";capitalize=").append(capitalize ? ScriptUtils.TRUE : ScriptUtils.FALSE) //
                    .append(";escape=").append(escapeWiki ? ScriptUtils.TRUE : ScriptUtils.FALSE) //
            ;
            if (sortPaths != null) {
                builder.append(";sortPaths=").append(sortPaths);
            }
            if (fieldOrderRegexPatterns != null) {
                builder.append(";fieldOrderRegexPatterns=").append(fieldOrderRegexPatterns);
            }
            builder.append("):BODY:") //
                    .append(trimmedValue) //
                    .append(":END:") //
            ;
            result = builder.toString();
            // log.debug("result: {}", result);
        }
        return result;
    }

    // NOTE: it was helpful for testing to use the Macro shorthand version, but I think going to directly to wiki markup should work better
    /**
     * For wiki and elements that are JSON, convert to another json-table macro to get a reasonable expansion
     * 
     * @param value
     * @return
     */
    @Override
    protected String getDefaultReplacement(final String value) {
        String result = value;
        String trimmedValue = value.trim();

        if (isWiki) {
            // TBL-461
            if (isWikilink(value)) {
                return trimmedValue;
            } else if (JsonUtils.isJson(value)) {

                boolean stripQualifiers = getBoolean("stripqualifiers", false, info); // strip path qualifiers in heading rows
                boolean capitalize = getBoolean("capitalize", true, info); // capitalize first char in header rows by default

                StringBuilder builder = new StringBuilder();
                builder.append("{json-table:output=wiki") //
                        .append("|stripQualifiers=").append(stripQualifiers ? ScriptUtils.TRUE : ScriptUtils.FALSE) //
                        .append("|capitalize=").append(capitalize ? ScriptUtils.TRUE : ScriptUtils.FALSE) //
                        .append("|escape=").append(escapeWiki ? ScriptUtils.TRUE : ScriptUtils.FALSE) //
                ;
                if (sortPaths != null) {
                    builder.append("|sortPaths=").append(sortPaths);
                }
                if (fieldOrderRegexPatterns != null) {
                    builder.append("|fieldOrderRegexPatterns=").append(fieldOrderRegexPatterns);
                }
                builder.append("}") //
                        .append(trimmedValue) //
                        .append("{json-table}") //
                ;
                result = builder.toString();
            }
        }
        return result;
    }

    /**
     * TBL-461: Check if data is of wikilink pattern
     * 
     * @param data
     * @return
     */
    private boolean isWikilink(String data) {
        data = data.trim();
        boolean part1Matched = false;
        boolean part2Matched = false;
        if (data.startsWith("[") && data.endsWith("]")) {
            if (data.indexOf("|") > 0) {
                String part1 = data.substring(1, data.indexOf("|") - 1).trim();
                String part2 = data.substring(data.indexOf("|") + 1, data.length() - 1).trim();
                log.debug("part1:{}, part2:{}", part1, part2);
                Pattern part1Pattern = Pattern.compile("^[^\\[|]*$");
                part1Matched = part1Pattern.matcher(part2).find();
                Pattern part2Pattern = Pattern.compile("^(http(s)?|ftp|ssh):[\\S]*");
                part2Matched = part2Pattern.matcher(part2).find();
                log.debug("part1Matched:{}, part2Matched:{}", part1Matched, part2Matched);
            }
        }
        return part1Matched && part2Matched;
    }

}
