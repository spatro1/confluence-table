/*
 * Copyright (c) 2005, 2013 Bob Swift Software, LLC.
 * All rights reserved.
 *
 * This software is licensed under the provisions of the "Standard EULA" from the
 * "Atlassian Marketplace Terms of Use" as a "Marketplace Product". Reference is:
 * http://www.atlassian.com/licensing/marketplace/termsofuse.
 * The this case, the "Publisher" is Bob Swift Software, LLC
 * and the "Marketplace Product" is Table Plugin for Confluence.
 *
 * See the LICENSE file for more details.
 */

/**
 * Copyright (c) 2005, 2012 Bob Swift
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *              notice, this list of conditions and the following disclaimer in the
 *            documentation and/or other materials provided with the distribution.
 *     * The names of contributors may not be used to endorse or promote products
 *           derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Created on Dec 3, 2005 by Bob Swift
 * - includes code based on work from Danny Chen and David Peterson
 */

package org.swift.confluence.table;

import java.io.InputStream;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.swift.confluence.macrosecurity.utils.MacroSecurityUtils;
import org.swift.confluence.macroutil.MacroInfo;
import org.swift.confluence.scriptutil.LicensedScriptMacro;
import org.swift.confluence.scriptutil.ScriptUtils;
import org.swift.confluence.scriptutil.TableHelper;
import org.swift.confluence.tablesorter.TableSorter;
import org.swift.confluence.tablesorter.TableUtil;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.templates.PageTemplateManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.SubRenderer;
import com.atlassian.upm.api.license.PluginLicenseManager;
import com.atlassian.webresource.api.assembler.PageBuilderService;

/**
 * This macro takes CSV data and produces a html or wiki table.
 *
 * <pre>
 * CSV - comma separated values
 * - CSV is not a formal standard, but the best reference is http://www.creativyst.com/Doc/Articles/CSV/CSV01.htm
 * - using regex plus some tweaking, we can come close to following this standard, following it exactly would probably
 *   take significantly longer
 * - most things are covered based on the reference above
 *   1) each record is a line and quoted lines splits are OK
 *   2) fields separated by commas
 *      - extended to whitespace separated and some other delimiters for convenience, but quoting may not work in these case
 *   3) space-characters adjacent to comma field separators are ignored
 *   4) Fields with embedded commas must be delimited with double-quote characters.
 *   5) Fields that contain double quote characters must be surrounded by double-quotes,
 *      and the embedded double-quotes must each be represented by a pair of consecutive double quotes
 *      - yes for most normal cases, but handling is simplistic and can easily be broken
 *   6) Fields with leading or trailing spaces must be delimited with double-quote characters
 *      - not really handled since the spaces disappear in the table rendering anyway
 *   7) Fields may always be delimited with double quotes - quotes will be discarded
 * </pre>
 */
public class CsvMacro extends LicensedScriptMacro {

    protected final boolean EXEMPT_DEVELOPER_LICENSE = true;
    protected final char LF = '\n';
    protected final char LF_MAC = '\r';
    static final protected Pattern PATTERN_REMOVE_P_TAGS = Pattern.compile("(<t[hd]\\s+[^>]*>)<p>([^<]*)</p>(</t[hd]>)");

    final protected PageBuilderService pageBuilderService; // for js resource
    // protected XhtmlContent xhtmlContent;
    final protected SubRenderer subRenderer;
    final protected BandanaManager bandanaManager;
    final protected UserAccessor userAccessor;

    protected static final RenderMode RENDER_MODE_MACROS = RenderMode.allow(RenderMode.F_MACROS | RenderMode.F_RESOLVE_TOKENS); // for macros = true

    /**
     * Constructor
     *
     * @param macroAssistant
     */
    protected CsvMacro(final SettingsManager settingsManager, final BootstrapManager bootstrapManager, final PageTemplateManager templateManager,
            final SpaceManager spaceManager, final PageManager pageManager, final PermissionManager permissionManager,
            final AttachmentManager attachmentManager, final LocaleManager localeManager, final I18NBeanFactory i18NBeanFactory,
            final PluginLicenseManager licenseManager, final PageBuilderService pageBuilderService, final XhtmlContent xhtmlContent,
            final SubRenderer subRenderer, final BandanaManager bandanaManager, final UserAccessor userAccessor) {
        super(settingsManager, bootstrapManager, templateManager, spaceManager, pageManager, permissionManager, attachmentManager, localeManager,
                i18NBeanFactory, licenseManager, xhtmlContent);
        this.pageBuilderService = pageBuilderService;
        this.subRenderer = subRenderer;
        this.bandanaManager = bandanaManager;
        this.userAccessor = userAccessor;
    }

    /**
     * Should we allow use for Confluence developer license even if no valid plugin license?
     *
     * @return true to allow use or false to report normal errors for invalid licenses
     */
    @Override
    protected boolean exemptDeveloperLicense() {
        return EXEMPT_DEVELOPER_LICENSE;
    }

    @Override
    protected String getExtension() {
        return ".csv";
    }

    @Override
    protected String getMacroName() {
        return "csv";
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.PLAIN_TEXT;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.INLINE;
    }

    @Override
    public String execute(final Map<String, String> params, final String body, final ConversionContext conversionContext) throws MacroExecutionException {
        // log.debug("link renderer from conversion context: " + conversionContext.getPageContext().getLinkRenderer());
        return super.execute(params, body, conversionContext);
    }

    /**
     * Primary processing
     */
    @Override
    protected String execute(MacroInfo info) throws MacroExecutionException {
        validateLicense(licenseManager);

        String delimiter = info.getMacroParams().getString("delimiter", ","); // default to comma separated
        if (!delimiter.startsWith(" ")) { // compatibility with old behavior (good or bad), leave blank as valid
            delimiter = delimiter.trim();
        }

        String url = info.getMacroParams().getString("url", null);
        String encoding = info.getMacroParams().getString("encoding", "");
        boolean macros = ScriptUtils.getBoolean("macros", false, info); // default to no macro rendering

        String data = ScriptUtils.replaceEditorBlank(info.getMacroBody()); // always include the body, add file stuff if specified
        try {
            data += getData(info, null, "script");

            if (url != null) {
                ScriptUtils.checkUrlIsAllowed(url, true); // validate, throw exception if not
                MacroSecurityUtils.securityCheck(bandanaManager, userAccessor, getMacroName(), info, "url", null);

                final InputStream stream = getUrlStream(info, null, url, false, getSessionCookie(info, url));
                data = data + ScriptUtils.getStreamAsString(stream, encoding);
                IOUtils.closeQuietly(stream);
            }
            if (macros) {
                data = subRenderer.render(data, info.getConversionContext().getPageContext(), RENDER_MODE_MACROS);
                log.debug("data: {}", data);
            }

            boolean isMacRoman = encoding.equalsIgnoreCase("MacRoman");

            String[] values = ScriptUtils.csvDataAsList(data.equals("") ? " " : data, delimiter, getQuoteFromParameter(info), isMacRoman ? LF_MAC : LF)
                    .toArray(new String[] {});

            TableSorter tableSorter = TableUtil.setupTableSorter(new TableHelper().getTableProperties(info), pageBuilderService);
            tableSorter = TableUtil.updateSortTip(tableSorter, getStringOrTranlation(tableSorter.getSortTip(), "@org.swift.confluence.table.sortTip"));
            CsvHelper csvHelper = new CsvHelper(subRenderer, tableSorter, null, values); // null - no parameters
            csvHelper.setAntiXssMode(isAntiXssMode(info));
            if (isMacRoman) {
                csvHelper.setEolChar(LF_MAC);
            }
            String tableId = TableUtil.generateTableId(info.getMacroParams().getString("id", null));
            csvHelper.setTableId(tableId);
            StringBuilder builder = new StringBuilder();
            builder.append(csvHelper.handleTableSorterAdditions(info, csvHelper.generateOutput(info)));

            // TBL-55: Columnstyles shall be applied later, after sticky headers loaded
            // builder.append(TableUtil.columnStyleHtml(new TableStyleHelper().constructColumnStyleHtml(ScriptUtils.getStyles(info, "columnStyles")), tableId));

            return builder.toString();// csvHelper.handleTableSorterAdditions(info, csvHelper.generateOutput(info));
        } catch (MacroExecutionException exception) { // these exceptions are already covered, so just pass them along
            // exception.printStackTrace();
            throw exception;
        } catch (Exception exception) {
            // exception.printStackTrace();
            log.error(info.getMacroBody(), exception); // log unexpected exception for problem determination
            throw new MacroExecutionException("Unexpected program error: " + exception.toString());
        }
    }

    /**
     * Get quote parameter
     *
     * @param info
     * @return quote or exception
     * @throws MacroExecutionException
     */
    protected char getQuoteFromParameter(final MacroInfo info) throws MacroExecutionException {
        String quote = info.getMacroParams().getString("quote", "\"").trim(); // default to double quote
        if (quote.equalsIgnoreCase("single")) {
            quote = "'";
        } else if (quote.equalsIgnoreCase("double")) {
            quote = "\"";
        } else if (quote.length() != 1) {
            throw new MacroExecutionException("Quote parameter must be a single character.");
        }
        return quote.charAt(0);
    }

    /**
     * Determine if antiXss mode is on or not
     *
     * @param info
     * @return
     */
    protected boolean isAntiXssMode(final MacroInfo info) throws MacroExecutionException {
        boolean isAntiXssMode = settingsManager.getGlobalSettings().isAntiXssMode();

        if (ScriptUtils.getBoolean("disableantixss", false, info)) { // if set to true and authorized, then reset
            if (MacroSecurityUtils.securityCheck(bandanaManager, userAccessor, getMacroName(), info, "disableAntiXss", null)) {
                isAntiXssMode = false;
            }
        }
        return isAntiXssMode;
    }
}
