/*
 * Copyright (c) 2014 Appfire Technologies, Inc.
 * All rights reserved.
 *
 * This software is licensed under the provisions of the "Standard EULA" from the
 * "Atlassian Marketplace Terms of Use" as a "Marketplace Product". Reference is:
 * http://www.atlassian.com/licensing/marketplace/termsofuse.
 * The this case, the "Publisher" is Appfire Technologies, Inc
 * and the "Marketplace Product" is Table Plugin for Confluence.
 *
 * See the LICENSE file for more details.
 */

package org.swift.confluence.table;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.swift.confluence.macrosecurity.utils.MacroSecurityUtils;
import org.swift.confluence.macroutil.MacroInfo;
import org.swift.confluence.scriptutil.Constants;
import org.swift.confluence.scriptutil.LicensedScriptMacro;
import org.swift.confluence.scriptutil.ScriptUtils;
import org.swift.confluence.scriptutil.TableHelper;
import org.swift.confluence.tablesorter.TableSorter;
import org.swift.confluence.tablesorter.TableUtil;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.templates.PageTemplateManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.SubRenderer;
import com.atlassian.upm.api.license.PluginLicenseManager;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.jayway.jsonpath.JsonPath;

/**
 * This macro creates a csv table from attachment data based on various filters
 */
public class JsonTableMacro extends LicensedScriptMacro {

    protected final boolean EXEMPT_DEVELOPER_LICENSE = true;

    final protected PageBuilderService pageBuilderService; // for js resource
    final protected SubRenderer subRenderer;
    final protected BandanaManager bandanaManager;
    final protected UserAccessor userAccessor;

    protected static final RenderMode RENDER_MODE_MACROS = RenderMode.allow(RenderMode.F_MACROS | RenderMode.F_RESOLVE_TOKENS); // for macros = true

    protected static final RenderMode RENDER_MODE_WIKI = RenderMode.suppress(RenderMode.F_FIRST_PARA); // for output
    protected static final String DEFAULT_COLUMNS_SINGLE = "file,size,creator,created,comment";
    protected static final String DEFAULT_COLUMNS = "page," + DEFAULT_COLUMNS_SINGLE;
    protected static final int DEFAULT_LIMIT = 1000; // number of attachments limit, should match default for macro browser

    /**
     * Constructor
     */
    protected JsonTableMacro(final SettingsManager settingsManager, final BootstrapManager bootstrapManager, final PageTemplateManager templateManager,
            final SpaceManager spaceManager, final PageManager pageManager, final PermissionManager permissionManager,
            final AttachmentManager attachmentManager, final LocaleManager localeManager, final I18NBeanFactory i18NBeanFactory,
            final PluginLicenseManager licenseManager, final PageBuilderService pageBuilderService, final XhtmlContent xhtmlContent,
            final SubRenderer subRenderer, final BandanaManager bandanaManager, final UserAccessor userAccessor) {
        super(settingsManager, bootstrapManager, templateManager, spaceManager, pageManager, permissionManager, attachmentManager, localeManager,
                i18NBeanFactory, licenseManager, xhtmlContent);
        this.pageBuilderService = pageBuilderService;
        this.subRenderer = subRenderer;
        this.bandanaManager = bandanaManager;
        this.userAccessor = userAccessor;
    }

    /**
     * Should we allow use for Confluence developer license even if no valid plugin license?
     *
     * @return true to allow use or false to report normal errors for invalid licenses
     */
    @Override
    protected boolean exemptDeveloperLicense() {
        return EXEMPT_DEVELOPER_LICENSE;
    }

    @Override
    protected String getExtension() {
        return ".json";
    }

    @Override
    protected String getMacroName() {
        return "json-table";
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.PLAIN_TEXT;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.INLINE;
    }

    @Override
    public String execute(final Map<String, String> params, final String body, final ConversionContext conversionContext) throws MacroExecutionException {
        // log.debug("link renderer from conversion context: " + conversionContext.getPageContext().getLinkRenderer());
        return super.execute(params, body, conversionContext);
    }

    /**
     * Primary processing
     */
    @Override
    protected String execute(MacroInfo info) throws MacroExecutionException {
        validateLicense(licenseManager);

        String paths = info.getMacroParams().getString("paths", "$").trim();
        if (paths.equals("")) {
            paths = "$";  // this default works because we now support non array json
        }
        String fieldPaths = info.getMacroParams().getString("fieldpaths", null);
        String fieldOrderRegexPatterns = info.getMacroParams().getString("fieldorderregexpatterns", null);
        String sortPaths = info.getMacroParams().getString("sortpaths", null);

        boolean sortDescending = ScriptUtils.getBoolean("sortdescending", false, info);
        boolean stripQualifiers = ScriptUtils.getBoolean("stripqualifiers", false, info); // strip path qualifiers in heading rows
        boolean capitalize = ScriptUtils.getBoolean("capitalize", true, info); // capitalize first char in header rows by default
        boolean escapeWiki = ScriptUtils.getBoolean("escape", false, info); // should we escape wiki in non-JSON strings
        boolean isWiki = "wiki".equalsIgnoreCase(info.getMacroParams().getString("output", "html")); // are we doing wiki rendering

        String url = info.getMacroParams().getString("url", null);
        String encoding = info.getMacroParams().getString("encoding", "");
        boolean macros = ScriptUtils.getBoolean("macros", false, info); // default to no macro rendering
        String[] idList = info.getMacroParams().getString("id", "").split(","); // default to automatically generate id

        List<JsonPath> pathList = JsonUtils.getPathList(paths);  // could be null
        List<JsonPath> fieldPathList = JsonUtils.getPathList(fieldPaths);
        List<JsonPath> sortPathList = JsonUtils.getPathList(sortPaths);
        List<Pattern> fieldOrderPatternList = JsonUtils.getPatternList(fieldOrderRegexPatterns);

        StringBuilder builder = new StringBuilder();
        try {
            String data = getData(info, null, "script").trim();
            if ((data.length() == 0) && (url != null)) {
                ScriptUtils.checkUrlIsAllowed(url, true); // validate, throw exception if not
                MacroSecurityUtils.securityCheck(bandanaManager, userAccessor, getMacroName(), info, "url", null);

                final InputStream stream = getUrlStream(info, null, url, true, getSessionCookie(info, url));
                // final InputStream stream = ScriptUtils.getUrlStream(info, url, true, null);
                data = ScriptUtils.getStreamAsString(stream, encoding).trim();
                IOUtils.closeQuietly(stream);
            }
            if (data.length() == 0) {
                data = ScriptUtils.replaceEditorBlank(info.getMacroBody()).trim(); // perhaps there is data in the macro body
            }

            if (macros) {
                data = subRenderer.render(data, info.getConversionContext().getPageContext(), RENDER_MODE_MACROS).trim();
                // log.debug("data: {}", data);
            }
            if (data.length() > 0) {  // found some JSON data, it is already trimmed

                data = JsonUtils.handleJsonP(data); // subset to json found within a JSONP formatted string

                log.debug("data size: {}", data.length());
                // log.debug("data: {}", data); // don't log data unless debugging
                int tableIdIndex = 0;  // loop through table ids, one for each table to be processed

                StringBuilder columnStyleHtml = new StringBuilder();
                // New table for each path specified
                for (JsonPath jsonPath : pathList) {

                    String tableId = null;
                    if ((tableIdIndex < idList.length) && (idList[tableIdIndex].trim().length() > 0)) {
                        tableId = GeneralUtil.htmlEncode(idList[tableIdIndex].trim());
                    }
                    tableIdIndex++;  // increment to next table id index

                    String csv = null;
                    try {
                        csv = JsonUtils.convertJsonToCsv(data, jsonPath, fieldPathList, sortPathList, fieldOrderPatternList, sortDescending, capitalize,
                                stripQualifiers, isWiki, escapeWiki);
                    } catch (Exception exception) {
                        throw new MacroExecutionException("Error parsing JSON. " + JsonUtils.getErrorMessage(exception));
                    }
                    // log.debug("csv: {}", csv);

                    // The result may be null if the the array is empty for example
                    if (!StringUtils.isBlank(csv)) {
                        boolean isMacRoman = encoding.equalsIgnoreCase("MacRoman");

                        String[] values = ScriptUtils.csvDataAsList(data.equals("") ? " " : csv, ",", '"', isMacRoman ? Constants.LF_MAC : Constants.LF)
                                .toArray(new String[] {});

                        Map<String, Object> parameters = new HashMap<String, Object>();
                        parameters.put("escape", "false"); // we are going to handle escape specifically here on non-JSON values only

                        // use json specif helper for automatic json sub-element handling, use null for no parameters
                        TableSorter tableSorter = TableUtil.setupTableSorter(new TableHelper().getTableProperties(info), pageBuilderService);
                        tableSorter = TableUtil.updateSortTip(tableSorter,
                                getStringOrTranlation(tableSorter.getSortTip(), "@org.swift.confluence.table.sortTip"));
                        CsvHelperForJsonTable csvHelper = new CsvHelperForJsonTable(info, subRenderer, tableSorter, parameters, values);
                        csvHelper.setSortPaths(sortPaths);
                        csvHelper.setSortPaths(fieldOrderRegexPatterns);
                        csvHelper.setAntiXssMode(isAntiXssMode(info));
                        if (tableId == null || (tableId != null && tableId.isEmpty())) {
                            tableId = TableUtil.generateTableId(tableId);
                        }
                        csvHelper.setTableId(tableId);  // use null to get default behavior
                        csvHelper.setEscapeWiki(escapeWiki); // pass this through
                        if (isMacRoman) {
                            csvHelper.setEolChar(Constants.LF_MAC);
                        }
                        builder.append(csvHelper.handleTableSorterAdditions(info, csvHelper.generateOutput(info)));
                        // TBL-55: Columnstyles shall be applied later, after sticky headers loaded
                        // columnStyleHtml.append(TableUtil
                        // .columnStyleHtml(new TableStyleHelper().constructColumnStyleHtml(ScriptUtils.getStyles(info, "columnStyles")), tableId));
                    } else {
                        // nothing to added (empty)
                    }
                }
                // builder.append(columnStyleHtml);
            }
        } catch (MacroExecutionException exception) { // these exceptions are already covered, so just pass them along
            exception.printStackTrace();
            throw exception;
        } catch (Exception exception) {
            exception.printStackTrace();
            log.error(info.getMacroBody(), exception); // log unexpected exception for problem determination
            throw new MacroExecutionException("Unexpected program error: " + exception.toString());
        }
        // log.debug("table:{}", builder.toString());
        return builder.toString();
    }

    /**
     * Determine if antiXss mode is on or not
     *
     * @param info
     * @return
     */
    protected boolean isAntiXssMode(final MacroInfo info) throws MacroExecutionException {
        boolean isAntiXssMode = settingsManager.getGlobalSettings().isAntiXssMode();

        if (ScriptUtils.getBoolean("disableantixss", false, info)) { // if set to true and authorized, then reset
            if (MacroSecurityUtils.securityCheck(bandanaManager, userAccessor, getMacroName(), info, "disableAntiXss", null)) {
                isAntiXssMode = false;
            }
        }
        return isAntiXssMode;
    }
}
