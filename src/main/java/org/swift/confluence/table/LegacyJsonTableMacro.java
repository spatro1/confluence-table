/*
 * Copyright (c) 2012 Bob Swift.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *              notice, this list of conditions and the following disclaimer in the
 *            documentation and/or other materials provided with the distribution.
 *     * The names of contributors may not be used to endorse or promote products
 *           derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  Created on: Mar 24, 2012
 *      Author: bob
 */

package org.swift.confluence.table;

import org.swift.confluence.scriptutil.LegacyConfluenceMacro;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.templates.PageTemplateManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.SubRenderer;
import com.atlassian.upm.api.license.PluginLicenseManager;
import com.atlassian.webresource.api.assembler.PageBuilderService;

public class LegacyJsonTableMacro extends LegacyConfluenceMacro {

    protected final JsonTableMacro newMacro;

    LegacyJsonTableMacro(final SettingsManager settingsManager, final BootstrapManager bootstrapManager, final PageTemplateManager templateManager,
            final SpaceManager spaceManager, final PageManager pageManager, final PermissionManager permissionManager,
            final AttachmentManager attachmentManager, final LocaleManager localeManager, final I18NBeanFactory i18NBeanFactory,
            final PluginLicenseManager licenseManager, final PageBuilderService webResourceManaager, final XhtmlContent xhtmlContent,
            final SubRenderer subRenderer, final BandanaManager bandanaManager, final UserAccessor userAccessor) {
        newMacro = new JsonTableMacro(settingsManager, bootstrapManager, templateManager, spaceManager, pageManager, permissionManager, attachmentManager,
                localeManager, i18NBeanFactory, licenseManager, webResourceManaager, xhtmlContent, subRenderer, bandanaManager, userAccessor);
    }

    @Override
    public RenderMode getBodyRenderMode() {
        return RenderMode.NO_RENDER;
    }

    @Override
    protected Macro getXHtmlMacro() {
        return newMacro;
    }

}
