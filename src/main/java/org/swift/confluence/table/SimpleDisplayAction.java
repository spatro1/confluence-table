/*
 * Copyright (c) 2014 Appfire Technologies, Inc.
 * All rights reserved.
 *
 * This software is licensed under the provisions of the "Standard EULA" from the
 * "Atlassian Marketplace Terms of Use" as a "Marketplace Product". Reference is:
 * http://www.atlassian.com/licensing/marketplace/termsofuse.
 * The this case, the "Publisher" is Appfire Technologies, Inc.
 *
 * See the LICENSE file for more details.
 */

package org.swift.confluence.table;

import com.atlassian.confluence.core.ConfluenceActionSupport;

/**
 * Action to show a get started dialog - this class is duplicated in various add-ons.
 */
public class SimpleDisplayAction extends ConfluenceActionSupport {
    private static final long serialVersionUID = 1L;

    /**
     * Called before screen is shown
     *
     * @return
     */
    public String input() {
        return INPUT;
    }
}
