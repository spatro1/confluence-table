/*
 * Copyright (c) 2005, 2013 Bob Swift Software, LLC.
 * All rights reserved.
 *
 * This software is licensed under the provisions of the "Standard EULA" from the
 * "Atlassian Marketplace Terms of Use" as a "Marketplace Product". Reference is:
 * http://www.atlassian.com/licensing/marketplace/termsofuse.
 * The this case, the "Publisher" is Bob Swift Software, LLC
 * and the "Marketplace Product" is Table Plugin for Confluence.
 *
 * See the LICENSE file for more details.
 */

package org.swift.confluence.table;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.swift.confluence.macroutil.MacroInfo;
import org.swift.confluence.scriptutil.DateHelper;
import org.swift.confluence.scriptutil.ScriptUtils;

import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.attachments.ImageDetailsManager;
import com.atlassian.confluence.pages.thumbnail.ThumbnailManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.User;

/**
 * Help construct attachment list and values based on various filters
 */
public class AttachmentTableHelper {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    // Special characters cause wiki link problems, handle separately
    static protected String SPECIAL_CHARS = ":@/\\|^#;[]{}<>$&+,=?"; // https://confluence.atlassian.com/display/CONFKB/Confluence+Page+URL's+Contain+pageId+Instead+of+The+Page+Title

    protected static final RenderMode RENDER_MODE_WIKI = RenderMode.suppress(RenderMode.F_FIRST_PARA); // for output

    static protected final String ALL_COLUMNS = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22"; // ,23";
    static protected final String[] ALL_COLUMNS_ARRAY = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18",
            "19", "20", "21", "22"}; // , "23"};

    protected final UserAccessor userAccessor;
    protected final DateHelper dateHelper;
    protected final int[] columnList;
    protected final String dateFormat;
    protected final SettingsManager settingsManager;
    protected final I18nResolver i18n;
    protected final PermissionManager permissionManager;
    protected final User currentUser = AuthenticatedUserThreadLocal.get();

    protected Pattern attachmentPattern; // for matching on attachment names
    protected Pattern commentPattern; // for matching on attachment comments
    protected Pattern labelPattern; // for matching on attachment labels

    protected String labelMatchOption;
    static public String MATCH_OPTION_SINGLE = "single"; // for matching on each label
    static public String MATCH_OPTION_ALL = "all"; // for matching on entire label list

    static protected final TableColumn[] tableColumns = {new TableColumn("", "S"), // 0 - empty column
            new TableColumn("page", "S"), // 1 - "Page" - page link
            new TableColumn("file", "S"), // 2 - file link - download or view
            new TableColumn("size", "I"), // 3
            new TableColumn("creator", "S"), // 4 - user link
            new TableColumn("created", "M"), // 5
            new TableColumn("comment", "S"), // 6
            new TableColumn("labels", "S"), // 7 - link to attachment properties was 22
            new TableColumn("version", "I"), // 8
            new TableColumn("type", "S"), // 9
            new TableColumn("mimetype", "S"), // 10
            new TableColumn("size2", "C"), // 11
            new TableColumn("modifier", "S"), // 12 - user link
            new TableColumn("modified", "M"), // 13
            new TableColumn("id", "I"), // 14
            new TableColumn("space", "S"), // 15 - space key
            new TableColumn("spacename", "S"), // 16 - space name
            new TableColumn("pagetitle", "S"), // 17 - page title (no link)
            new TableColumn("filename", "S"), // 18 - file name (no link)
            new TableColumn("thumbnail", "E"), // 19 - thumbnail link if available for the image
            new TableColumn("image", "E"), // 20 - image link
            new TableColumn("location", "S"), // 21 - link to location on page attachments list
            new TableColumn("properties", "S"), // 22 - link to attachment properties
    // new TableColumn("edit", "Edit", "S"), // 23 - edit in office

    };

    /**
     * Constructor
     */
    public AttachmentTableHelper(final SettingsManager settingsManager, final I18nResolver i18n, final PermissionManager permissionManager,
            final UserAccessor userAccessor, final DateHelper dateHelper, final String[] columnList, final String dateFormat) {
        this.dateHelper = dateHelper;
        this.columnList = convertToColumnNumberList(columnList);
        this.userAccessor = userAccessor;
        this.dateFormat = dateFormat;
        this.settingsManager = settingsManager;
        this.permissionManager = permissionManager;
        this.i18n = i18n;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setAttachmentPattern(final Pattern pattern) {
        attachmentPattern = pattern;
    }

    public void setCommentPattern(final Pattern pattern) {
        commentPattern = pattern;
    }

    public void setLabelPattern(final Pattern pattern) {
        labelPattern = pattern;
    }

    public void setLabelMatchOption(final String option) {
        labelMatchOption = option;
    }

    static protected int[] convertToColumnNumberList(final String[] nameList) {
        int[] numberList = new int[nameList.length];
        for (int i = 0; i < numberList.length; i++) {
            numberList[i] = lookupColumnNumber(nameList[i]);
        }
        return numberList;
    }

    /**
     * Generate attachment values specific to the list of columns requested
     */
    protected String[] generateAttachmentValues(final List<Attachment> attachmentList) {

        String[] values = new String[(attachmentList.size() + 1) * columnList.length]; // add 1 for heading row
        int index = 0;  // index in values array

        for (int i = 0; i < columnList.length; i++) {
            values[index] = tableColumns[columnList[i]].getHeading(i18n);
            index++; // next values index
        }
        for (Attachment attachment : attachmentList) {
            for (int i = 0; i < columnList.length; i++) {
                values[index] = getAttachmentValue(attachment, columnList[i]);
                index++; // next values index
            }
        }
        return values;
    }

    /**
     * Get attachment value based on column requested
     *
     * @param attachment
     * @param column
     * @return value or blank if not found
     */
    protected String getAttachmentValue(final Attachment attachment, final int columnNumber) {
        // int columnNumber = lookupColumnNumber(column);

        Page page = (attachment.getContent() instanceof Page) ? (Page) attachment.getContent() : null;

        switch (columnNumber) { // 1 based column number
        case 1:
            return getPageLinkWiki(attachment, page);
        case 2:
            return getAttachmentLinkWiki(attachment, page, true);
        case 3:
            return Long.toString(attachment.getFileSize());
        case 4:
            return getUserWikiEntry(attachment.getCreator());
        case 5:
            return getDateEntry(attachment.getCreationDate());
        case 6:
            return ScriptUtils.ensureWikiValue(attachment.getComment());
        case 7:
            StringBuilder builder = new StringBuilder();
            for (Label label : attachment.getLabelsForDisplay(AuthenticatedUserThreadLocal.get())) {
                builder.append("[").append(label).append("|").append(getBaseUrl()).append("/label/").append(label).append("] ");
            }
            return builder.toString();
        case 8:
            return attachment.getAttachmentVersion().toString();
        case 9:
            return ScriptUtils.ensureWikiValue(attachment.getNiceType());  // handles null as well
        case 10:
            return ScriptUtils.ensureWikiValue(attachment.getContentType());
        case 11:
            return attachment.getNiceFileSize();
        case 12:
            return getUserWikiEntry(attachment.getLastModifier());
        case 13:
            return getDateEntry(attachment.getLastModificationDate());
        case 14:
            return Long.toString(attachment.getId());
        case 15:
            return attachment.getSpace().getKey();
        case 16:
            return ScriptUtils.ensureWikiValue(attachment.getSpace().getName());
        case 17:
            if (page != null) {
                return ScriptUtils.ensureWikiValue(page.getDisplayTitle());
            }
        case 18:
            return getAttachmentTitle(attachment, false);
        case 19: // thumbnail
        case 20: // image
            if (page != null) {
                if (attachment.getContentType().startsWith("image/")) {
                    if (!StringUtils.isBlank(attachment.getFileExtension())) {  // work around https://jira.atlassian.com/browse/CONF-9733
                        return getImageMarkup(page, attachment, columnNumber == 19);
                    }
                    log.warn("Attachment with id: {} has an image mime type but no file extension. Image could not be shown because of CONF-9733",
                            attachment.getId());
                }
            }
            break;
        case 21: // location
            return "[" + getAttachmentTitle(attachment, true) + "|" + getBaseUrl() + attachment.getUrlPath() + "]";
        case 22: {
            String attachmentUrl = attachment.getUrlPath();
            if (attachmentUrl == null) { // shouldn't happen
                return "";
            }
            attachmentUrl = attachmentUrl.replace("/viewpageattachments", "/editattachment").replace("highlight=", "fileName=").replaceFirst("#.*", "");
            return "[" + getAttachmentTitle(attachment, true) + "|" + getBaseUrl() + attachmentUrl + "]";
        }
        // case 23:
        // String fileName = attachment.getFileName();
        // String extension = FilenameUtils.getExtension(fileName).toLowerCase();
        // if (extension.startsWith("doc") || extension.startsWith("ppt") || extension.startsWith("xls")) {
        // return "[" + "Edit in Office" + "|" + getBaseUrl() + "/plugins/servlet/confluence/editinword/" + page.getId() + "/attachments/" + fileName
        // + "]";
        // }
        // break; // leave blank

        default:
        }
        return ""; // default is blank
    }

    /**
     * Local function to prepare a string to be included in wiki link markup. http://www.ascii.cl/htmlcodes.htm
     *
     * @return value that will not break wiki link markup
     */
    protected String ensureWikiValue(final String value) {
        return ScriptUtils.ensureWikiValue(value.replace("[", "_").replace("|", "_").replace("]", "_").replace("^", "_").replace(":", "_"));
    }

    /**
     * Ensure the title can be used in wiki markup. Use forLinkMarkup to make sure it works in a link markup.
     *
     * @param attachment
     * @param forLinkMarkup
     * @return
     */
    protected String getAttachmentTitle(final Attachment attachment, boolean forLinkMarkup) {
        return forLinkMarkup ? ensureWikiValue(attachment.getDisplayTitle()) : ScriptUtils.ensureWikiValue(attachment.getDisplayTitle());
    }

    protected String getPageLinkWiki(final Attachment attachment, final Page page) {
        String wiki;
        // if (page == null) ? ScriptUtils.ensureWikiValue(attachment.getContent().getDisplayTitle()) : page.getLinkWikiMarkup();
        if (page == null) {
            wiki = getAttachmentTitle(attachment, true);
        } else if (StringUtils.containsAny(page.getTitle(), SPECIAL_CHARS)) {
            wiki = "[" + ensureWikiValue(page.getTitle()) + "|" + getBaseUrl() + page.getUrlPath() + "]";
        } else {
            wiki = page.getLinkWikiMarkup(); // old standard way
        }
        log.debug("wiki: {}", wiki);
        return wiki;
    }

    protected String getAttachmentLinkWiki(final Attachment attachment, final Page page, boolean download) {
        String wiki;
        if (StringUtils.containsAny(attachment.getDisplayTitle(), SPECIAL_CHARS) || StringUtils.containsAny(page.getTitle(), SPECIAL_CHARS)) {
            wiki = "[" + getAttachmentTitle(attachment, true) + "|" + getBaseUrl() + (download ? attachment.getDownloadPath() : attachment.getUrlPath()) + "]";
        } else {
            wiki = attachment.getLinkWikiMarkup(); // old standard way
        }
        log.debug("wiki: {}", wiki);
        return wiki;
    }

    /**
     * Generate column types specific to the list of columns requested
     */
    protected List<String> generateColumnTypes() {

        List<String> columnTypes = new ArrayList<String>(columnList.length);
        for (int i = 0; i < columnList.length; i++) {
            columnTypes.add(tableColumns[columnList[i]].getColumnType());
        }
        log.debug("columnTypes: {}", columnTypes);
        return columnTypes;
    }

    /**
     * Lookup column number for a column. 1 based, 0 indicates column not found or blank. Use only the name field.
     *
     * @param column - lowercased and trimmed column name
     * @return 1 based column number or 0 if not found
     */
    static protected int lookupColumnNumber(String column) {

        column = column.trim().replace(" ", "").toLowerCase();  // normalize to all lower case with no blanks
        if (!column.equals("")) {
            if (StringUtils.isNumeric(column)) {
                int number = Integer.parseInt(column);
                if ((number > 0) && (number < tableColumns.length)) { // must be in range
                    return number;
                } else {
                    return 0;  // ignore invalid request
                }
            } else { // not a number, look up by name
                for (int i = 1; i < tableColumns.length; i++) {
                    if (column.equals(tableColumns[i].name)) {
                        return i;
                    }
                }
            }
        }
        return 0; // not found
    }

    /**
     * Get wiki markup for image
     *
     * @param page
     * @param attachment
     * @param thumbnail
     * @return wiki markup for an image
     */
    protected String getImageMarkup(final Page page, final Attachment attachment, boolean thumbnail) {
        StringBuilder builder = new StringBuilder();
        if (!thumbnail) {
            // TBL-485, if image found for attachement, then build mark-up
            ImageDetailsManager imageDetailsManager = (ImageDetailsManager) ContainerManager.getComponent("imageDetailsManager");
            if (imageDetailsManager != null && imageDetailsManager.getImageDetails(attachment) != null) {
                builder.append('!').append(page.getSpace().getKey()).append(':').append(page.getTitle());
                builder.append('^').append(attachment.getDisplayTitle());
                builder.append('!');
            }
        } else {
            // TBL-485: If attachment is thumbnailable, then only build respective wiki-markup.
            ThumbnailManager thumbnailManager = (ThumbnailManager) ContainerManager.getComponent("thumbnailManager");
            if (thumbnailManager.isThumbnailable(attachment)) {
                builder.append('!').append(page.getSpace().getKey()).append(':').append(page.getTitle());
                builder.append('^').append(attachment.getDisplayTitle());
                builder.append("|thumbnail!");
            }
        }
        return builder.toString();
    }

    /**
     * Add the csv entry data ensuring wiki values are escaped
     *
     * @param builder
     * @param string
     */
    protected void getEntry(final String string) {
        ScriptUtils.ensureWikiValue(string);
    }

    /**
     * Add the user wiki entry
     *
     * @param user
     * @return
     */
    protected String getUserWikiEntry(final ConfluenceUser user) {
        return user == null ? "Anonymous" : user.getName() == null ? "" : "[~" + user.getName() + "]";
    }

    protected String getDateEntry(final Date date) {
        return dateHelper.formatDate(date, dateFormat);
    }

    /**
     * Get base url including context path - see https://developer.atlassian.com/pages/viewpage.action?pageId=2031787
     * 
     * @return
     */
    public String getBaseUrl() {
        return settingsManager.getGlobalSettings().getBaseUrl();
    }

    /**
     * Add selected attachment information for attachments on this page
     * 
     * @param builder
     * @param page
     * @param pagePattern - could be null
     * @param attachmentPattern
     * @param commentPattern
     * @param labelPattern
     */
    protected void processForPage(final MacroInfo info, final List<Attachment> list, final Page page, final Pattern pagePattern,
            final Pattern pageLabelPattern, int limit) {
        // long timeMark = ScriptUtils.getTimeMark("start process for page", 0);
        // long timeMark2 = ScriptUtils.getTimeMark("start permission for page", 0);
        if ((page != null) && isViewPermitted(page)) {

            // timeMark2 = ScriptUtils.getTimeMark("start matcher for page", timeMark2);
            // log.debug("process page: {}, pagePattern: {}", page.getTitle(), pagePattern);
            if ((pagePattern == null) || pagePattern.matcher(page.getTitle()).matches()) {
                if ((pageLabelPattern == null) || labelMatch(page.getLabels(), pageLabelPattern, labelMatchOption)) {

                    // timeMark2 = ScriptUtils.getTimeMark("end matcher for page", timeMark2);

                    for (Attachment attachment : getSortedAttachments(page)) {
                        if (selectAttachment(attachment, attachmentPattern, commentPattern, labelPattern)) {
                            list.add(attachment);
                        }
                        if (list.size() >= limit) {
                            break;
                        }
                    }
                    // timeMark2 = ScriptUtils.getTimeMark("end select attachment for page", timeMark2);
                }
            }
        } else {
            // log.debug("page not processed: {}", page.getTitle());
        }
        // timeMark = ScriptUtils.getTimeMark("finish process for page", timeMark);
    }

    /**
     * Sort each pages attachments by name
     * 
     * @param page
     * @return
     */
    protected List<Attachment> getSortedAttachments(final Page page) {
        List<Attachment> list = page.getLatestVersionsOfAttachments();
        Collections.sort(list, new Comparator<Attachment>() {
            @Override
            public int compare(final Attachment a1, final Attachment a2) {
                return a1.getTitle().compareTo(a2.getTitle());
            }
        });
        return list;
    }

    /**
     * Select attachment only if all defined criteria are met. A defined criteria is met if the element matchese the pattern. Only one label needs to match the
     * pattern
     * 
     * @param attachment
     * @param attachmentPattern
     * @param commentPattern
     * @param labelPattern
     * @return true if attachment is selected
     */
    protected boolean selectAttachment(final Attachment attachment, final Pattern attachmentPattern, final Pattern commentPattern, final Pattern labelPattern) {
        boolean select = true; // select unless one of the defined conditions is false
        if (attachmentPattern != null) {
            select = attachmentPattern.matcher(attachment.getTitle()).matches();
        }
        if (select && (commentPattern != null)) {
            String comment = attachment.getComment();
            if (comment == null) {
                comment = "";
            }
            select = commentPattern.matcher(comment).matches();
        }
        if (select && (labelPattern != null)) {
            List<Label> labels = attachment.getLabels();
            if (labels == null) {
                labels = new ArrayList<Label>();
            }
            select = labelMatch(labels, labelPattern, labelMatchOption);
        }
        log.debug("select: {}, for attachment: {}", select, attachment.getTitle());
        return select;
    }

    /**
     * Apply label matching logic to a list of labels
     * 
     * @param labels - labels to match from some content
     * @param labelPattern - regex pattern
     * @param labelMatchOption - user option on how to match
     * @return true if label match logic was successful
     */
    @SuppressWarnings("unchecked")
    protected boolean labelMatch(final List<Label> labels, final Pattern labelPattern, final String labelMatchOption) {
        boolean select;
        if (MATCH_OPTION_ALL.equals(labelMatchOption)) {
            StringBuilder builder = new StringBuilder();
            Collections.sort(labels); // sort them to make the regex easier
            for (Label label : labels) {
                builder.append(" ").append(label.getName());
            }
            log.debug("labels as string: {}", builder.toString());
            select = labelPattern.matcher(builder.toString()).matches();
        } else {
            select = false; // need to find at least one match
            for (Label label : labels) {
                if (labelPattern.matcher(label.getName()).matches()) {
                    select = true;
                    break;
                }
            }
        }
        return select;
    }

    /**
     * Optimized version to avoid getUser call causing performance problems when used with future macro
     */
    protected boolean isViewPermitted(final Page page) {
        return permissionManager.hasPermission(currentUser, Permission.VIEW, page);
    }
}
